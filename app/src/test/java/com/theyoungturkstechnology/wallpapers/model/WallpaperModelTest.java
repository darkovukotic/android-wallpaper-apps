package com.theyoungturkstechnology.wallpapers.model;


import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Created by dvukotic on 21.12.2017.
 */

@SuppressWarnings("DefaultFileTemplate")
public class WallpaperModelTest {
    private final int wallpaperRid = 1;

    @Test
    public void shouldHaveWallpaperId() {
        WallpaperModel wallpaper = new WallpaperModel(wallpaperRid);
        assertEquals(wallpaperRid, wallpaper.getRid());
    }
}
