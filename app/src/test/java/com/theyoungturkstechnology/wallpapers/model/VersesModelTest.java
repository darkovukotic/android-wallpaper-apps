package com.theyoungturkstechnology.wallpapers.model;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

/**
 * Created by dvukotic on 12.02.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public class VersesModelTest {
    @Rule public final ExpectedException exception = ExpectedException.none();

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getVersesAsString() throws Throwable {
        final String versesStr = "This is a verses string and it can be short or long...";

        VersesModel verses = new VersesModel(versesStr);

        assertEquals(versesStr, verses.getVersesStr());
    }

    @Test
    public void constructorDoesntAcceptNull() throws Throwable {
        exception.expect(IllegalArgumentException.class);
        new VersesModel(null);
    }

    @Test
    public void constructorDoesntAcceptEmptyString() throws Throwable {
        exception.expect(IllegalArgumentException.class);
        new VersesModel("");
    }
}