package com.theyoungturkstechnology.wallpapers.model;

import android.graphics.Typeface;

import com.theyoungturkstechnology.wallpapers.R;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 * Created by dvukotic on 06.02.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public class FontModelTest {
    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void constructorShouldNotAcceptNull() throws Exception {
        exception.expect(IllegalArgumentException.class);
        new FontModel(FontCode.LatinFont, null, 10);
    }
}