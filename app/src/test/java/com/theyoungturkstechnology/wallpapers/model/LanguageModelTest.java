package com.theyoungturkstechnology.wallpapers.model;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.junit.Assert.*;

/**
 * Created by dvukotic on 29.01.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public class LanguageModelTest {
    @Rule public final ExpectedException exception = ExpectedException.none();
    @Rule public MockitoRule mockito = MockitoJUnit.rule();

    @Test
    public void shouldHaveCodeAndName() throws Exception {
        LanguageModel language = new LanguageModel("en", "English", FontCode.LatinFont);

        assertEquals("en", language.getCode());
        assertEquals("English", language.getName());
        assertEquals(FontCode.LatinFont, language.getPreferredFontCode());
    }

}