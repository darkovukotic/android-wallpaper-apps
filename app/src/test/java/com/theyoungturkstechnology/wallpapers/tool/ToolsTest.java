package com.theyoungturkstechnology.wallpapers.tool;

import com.theyoungturkstechnology.wallpapers.tool.Tools;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Created by dvukotic on 28.12.2017.
 */

@SuppressWarnings("DefaultFileTemplate")
public class ToolsTest {

    @Test
    public void getRandomPositiveIntMostOfTheTimeShouldReturnDifferentValues() {
        int int1 = Tools.getRandomPositiveInt();
        int int2 = Tools.getRandomPositiveInt();
        int int3 = Tools.getRandomPositiveInt();

        assertNotEquals(int1, int2);
        assertNotEquals(int1, int3);
        assertNotEquals(int2, int3);
    }

    @Test
    public void getRandomPositiveIntPositiveValues() {
        int int1 = Tools.getRandomPositiveInt();
        int int2 = Tools.getRandomPositiveInt();
        int int3 = Tools.getRandomPositiveInt();

        assertTrue(int1 >= 0);
        assertTrue(int2 >= 0);
        assertTrue(int3 >= 0);
    }
}
