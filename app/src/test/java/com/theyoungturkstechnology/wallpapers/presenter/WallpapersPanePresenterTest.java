package com.theyoungturkstechnology.wallpapers.presenter;

import android.support.annotation.NonNull;

import com.theyoungturkstechnology.wallpapers.model.WallpaperModel;
import com.theyoungturkstechnology.wallpapers.logic.WallpapersLogic;
import com.theyoungturkstechnology.wallpapers.adapter.PreferencesAdapter;
import com.theyoungturkstechnology.wallpapers.adapter.WallpapersAdapter;
import com.theyoungturkstechnology.wallpapers.presenter.WallpapersPaneContract.View;
import com.theyoungturkstechnology.wallpapers.presenter.WallpapersPaneContract.Presenter;
import com.theyoungturkstechnology.wallpapers.repository.WallpapersCallback;
import com.theyoungturkstechnology.wallpapers.repository.WallpapersRepository;
import com.theyoungturkstechnology.wallpapers.type.CallbackResult;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.ArrayList;

import static org.mockito.Mockito.*;

/**
 * Created by dvukotic on 25.12.2017.
 */
@SuppressWarnings("DefaultFileTemplate")
public class WallpapersPanePresenterTest {
    @Rule public final ExpectedException exception = ExpectedException.none();
    @Rule public MockitoRule mockito = MockitoJUnit.rule();

    @Mock private PreferencesAdapter preferences;
    @Mock private WallpapersAdapter adapter;
    @Mock private WallpapersRepository repository;
    private View view;
    private ArrayList<WallpaperModel> listOf3BasicWallpapers;

    @Before
    public void setUp() throws Exception {
        listOf3BasicWallpapers = new ArrayList<>();
        listOf3BasicWallpapers.add(new WallpaperModel(1));
        listOf3BasicWallpapers.add(new WallpaperModel(2));
        listOf3BasicWallpapers.add(new WallpaperModel(3));

        view = new View() {
            @Override
            public void showInFlipperWallpaperWithIndex(int index) {}
            @Override
            public void showMessage(int messageRid) {}
            @Override
            public void setPresenter(@NonNull PanePresenter presenter) {}
        };
    }

    @Test
    public void shouldNotAcceptNullWallpapersObject() throws Exception {
        exception.expect(IllegalArgumentException.class);
        new WallpapersPanePresenter.Builder()
            .view(view)
            .preferences(preferences)
            .build();
    }

    @Test
    public void shouldSetSelectedWallpaper() throws Exception {
        WallpapersLogic wallpapers = new WallpapersLogic.Builder()
            .preferences(preferences)
            .adapter(adapter)
            .repository( (WallpapersCallback caller) -> caller.wallpapersCallback(CallbackResult.Success, listOf3BasicWallpapers) )
            .build();
        Presenter presenter = new WallpapersPanePresenter.Builder()
            .view(view)
            .preferences(preferences)
            .wallpapers(wallpapers)
            .build();

        int index = 0;
        presenter.selectedWallpaperWithIndex(index);

        WallpaperModel wallpaper = wallpapers.getWallpaperByIndex(index);
        verify(adapter).setWallpaper(wallpaper);

        index = 1;
        presenter.selectedWallpaperWithIndex(index);

        wallpaper = wallpapers.getWallpaperByIndex(index);
        verify(adapter).setWallpaper(wallpaper);

        index = 2;
        presenter.selectedWallpaperWithIndex(index);

        wallpaper = wallpapers.getWallpaperByIndex(index);
        verify(adapter).setWallpaper(wallpaper);
    }

    @Test
    public void onSelectedWallpaperItsIndexShouldBeSavedToPreferences() throws Exception {
        WallpapersLogic wallpapers = new WallpapersLogic.Builder()
            .preferences(preferences)
            .adapter(adapter)
            .repository(new WallpapersRepository() {
                @Override
                public void getWallpapers(WallpapersCallback caller) throws Exception {
                    caller.wallpapersCallback(CallbackResult.Success, listOf3BasicWallpapers);
                }
            })
            .build();
        Presenter presenter = new WallpapersPanePresenter.Builder()
            .view(view)
            .preferences(preferences)
            .wallpapers(wallpapers)
            .build();

        final int index = 1;
        presenter.selectedWallpaperWithIndex(index);

        verify(preferences).saveLastSelectedWallpaperIndex(index);
    }

}
