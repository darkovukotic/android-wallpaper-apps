package com.theyoungturkstechnology.wallpapers.presenter;

import android.support.annotation.NonNull;

import com.theyoungturkstechnology.wallpapers.adapter.MusicAdapter;
import com.theyoungturkstechnology.wallpapers.adapter.PreferencesAdapter;
import com.theyoungturkstechnology.wallpapers.adapter.SpeakingAdapter;
import com.theyoungturkstechnology.wallpapers.view.PaneType;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static com.theyoungturkstechnology.wallpapers.presenter.NavigationPaneContract.*;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 * Created by dvukotic on 14.02.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public class NavigationPanePresenterTest {
    @Rule public final ExpectedException exception = ExpectedException.none();
    @Rule public MockitoRule mockito = MockitoJUnit.rule();

    private NavigationPaneContract.View view;
    @Mock private PreferencesAdapter preferences;
    @Mock private MusicAdapter music;
    @Mock private SpeakingAdapter speaking;
    private NavigationPanePresenter presenter;

    @Before
    public void setUp() throws Exception {
        view = new NavigationPaneContract.View() {
            @Override
            public void loadPane(@NonNull PaneType pane) {}
            @Override
            public void setBackgroundImage(int rid) {}
            @Override
            public void showMessage(int messageRid) {}
            @Override
            public void setPresenter(@NonNull PanePresenter presenter) {}
        };

        presenter = new NavigationPanePresenter.Builder()
            .view(view)
            .build();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void shouldNotAcceptNullView() {
        exception.expect(IllegalArgumentException.class);
        new NavigationPanePresenter.Builder()
            .view(null)
            .build();
    }

    @Test
    public void onAppStartedShouldStartMusicWhenEnabled() {
        when(preferences.getIsMusicEnabled())
            .thenReturn(true);

        Presenter presenter = new NavigationPanePresenter.Builder()
            .view(view)
            .preferences(preferences)
            .music(music)
            .build();

        presenter.onAppActive();
        verify(music).play();
    }

    @Test
    public void onAppStartedShouldNotStartMusicWhenNotEnabled() {
        when(preferences.getIsMusicEnabled())
            .thenReturn(false);

        Presenter presenter = new NavigationPanePresenter.Builder()
            .view(view)
            .preferences(preferences)
            .music(music)
            .build();

        presenter.onAppActive();
        verifyZeroInteractions(music);
    }

    @Test
    public void onAppStartedShouldNotStartMusicWhenAppDoesntSupportIt() {
        Presenter presenter = new NavigationPanePresenter.Builder()
            .view(view)
            .preferences(preferences)
            .build();

        presenter.onAppActive();
        verifyZeroInteractions(music);
    }

    @Test
    public void onAppStoppedShouldStopMusic() throws Exception {
        when(preferences.getIsMusicEnabled())
            .thenReturn(true);

        Presenter presenter = new NavigationPanePresenter.Builder()
            .view(view)
            .preferences(preferences)
            .music(music)
            .build();

        presenter.onAppActive();
        verify(music).play();
        presenter.onAppInactive();
        verify(music).stop();
    }

    @Test
    public void onAppStoppedShouldStopSpeaking() throws Exception {
        Presenter presenter = new NavigationPanePresenter.Builder()
            .view(view)
            .speaking(speaking)
            .build();

        presenter.onAppInactive();
        verify(speaking).stop();
    }
}