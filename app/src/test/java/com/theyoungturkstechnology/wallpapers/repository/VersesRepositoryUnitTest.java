package com.theyoungturkstechnology.wallpapers.repository;

import com.theyoungturkstechnology.wallpapers.adapter.DBAdapter;
import com.theyoungturkstechnology.wallpapers.adapter.ServerAdapter;
import com.theyoungturkstechnology.wallpapers.MOCK.DBAdapterMock;
import com.theyoungturkstechnology.wallpapers.MOCK.ServerAdapterMock;
import com.theyoungturkstechnology.wallpapers.model.VersesModel;
import com.theyoungturkstechnology.wallpapers.model.VersesType;
import com.theyoungturkstechnology.wallpapers.type.CallbackResult;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.junit.Assert.*;

/**
 * Created by dvukotic on 12.03.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public class VersesRepositoryUnitTest {
    @Rule public final ExpectedException exception = ExpectedException.none();
    @Rule public MockitoRule mockito = MockitoJUnit.rule();

    private VersesRepository repository;
    @Mock private DBAdapter db;
    @Mock private ServerAdapter server;

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void withoutServerAdapterShouldFail() throws Throwable {
        exception.expect(IllegalArgumentException.class);
        repository = new VersesRepositoryObject.Builder()
            .database(db)
            .build();
    }

    @Test
    public void withoutDBAdapterShouldFail() throws Throwable {
        exception.expect(IllegalArgumentException.class);
        repository = new VersesRepositoryObject.Builder()
            .server(server)
            .build();
    }

    @Test
    public void shouldWorkWithMockObjects() throws Throwable {
        repository = new VersesRepositoryObject.Builder()
            .server(server)
            .database(db)
            .build();

        assertNotNull(repository);
    }

    @Test
    public void shouldWorkWithMockClasses() throws Throwable {
        repository = new VersesRepositoryObject.Builder()
            .server(new ServerAdapterMock())
            .database(new DBAdapterMock())
            .build();

        assertNotNull(repository);
    }

    @Test
    public void onStartGetCurrentVersesShouldReturnError() throws Throwable {
        repository = new VersesRepositoryObject.Builder()
            .server(new ServerAdapterMock())
            .database(new DBAdapterMock())
            .build();

        repository.getVerses(
            VersesType.Current,
            (CallbackResult result, VersesSource source, VersesType type, VersesModel verses) -> {
                assertEquals(VersesType.Current, type);
                assertEquals(CallbackResult.Error, result);
            }
        );
    }

}
