package com.theyoungturkstechnology.wallpapers.logic;

import com.theyoungturkstechnology.wallpapers.MOCK.LanguagesRepositoryMock;
import com.theyoungturkstechnology.wallpapers.MOCK.LocaleAdapterMock;
import com.theyoungturkstechnology.wallpapers.MOCK.PreferencesAdapterMock;
import com.theyoungturkstechnology.wallpapers.model.FontCode;
import com.theyoungturkstechnology.wallpapers.model.LanguageModel;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by dvukotic on 29.01.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public class LanguagesLogicTest {
    @Rule public final ExpectedException exception = ExpectedException.none();
    @Rule public MockitoRule mockito = MockitoJUnit.rule();
    private LanguagesLogic logic;

    @Before
    public void setUp() throws Exception {
        logic = new LanguagesLogic.Builder()
            .locale(new LocaleAdapterMock())
            .preferences(new PreferencesAdapterMock())
            .repository(new LanguagesRepositoryMock())
            .build();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void shouldGetCurrentLanguage() throws Exception {
        LanguageModel lang = new LanguageModel("en", "English", FontCode.LatinFont);
        logic.saveCurrentLanguage(lang);

        LanguageModel language = logic.getCurrentLanguage();
        assertEquals("en", language.getCode());
    }

    @Test
    public void shouldGetLanguages() throws Exception {
        List<LanguageModel> languageList = logic.getLanguages();
        assertNotNull(languageList);
        assertFalse(languageList.isEmpty());
    }

    @Test
    public void shouldHave6Languages() throws Exception {
        List<LanguageModel> languageList = logic.getLanguages();

        assertTrue(languageList != null && !languageList.isEmpty());
        assertEquals(6, languageList.size());
    }

    @Test
    public void shouldSaveCurrentLanguage() throws Exception {
        LanguageModel origLanguage = logic.getCurrentLanguage();
        assertNotNull(origLanguage);
        assertEquals("en", origLanguage.getCode());

        LanguageModel newLanguage = new LanguageModel("tr", "Turkish", FontCode.LatinFont);
        logic.saveCurrentLanguage(newLanguage);
        LanguageModel language = logic.getCurrentLanguage();
        assertNotNull(language);
        assertEquals("tr", language.getCode());
    }
}
