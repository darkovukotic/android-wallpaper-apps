package com.theyoungturkstechnology.wallpapers.logic;


import com.theyoungturkstechnology.wallpapers.exception.NoObjectOrEmptyException;
import com.theyoungturkstechnology.wallpapers.R;
import com.theyoungturkstechnology.wallpapers.logic.WallpapersLogic;
import com.theyoungturkstechnology.wallpapers.adapter.PreferencesAdapter;
import com.theyoungturkstechnology.wallpapers.adapter.WallpapersAdapter;
import com.theyoungturkstechnology.wallpapers.model.WallpaperModel;
import com.theyoungturkstechnology.wallpapers.repository.WallpapersCallback;
import com.theyoungturkstechnology.wallpapers.repository.WallpapersRepository;
import com.theyoungturkstechnology.wallpapers.type.CallbackResult;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.io.IOException;
import java.util.ArrayList;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Created by dvukotic on 22.12.2017.
 */

@SuppressWarnings("DefaultFileTemplate")
public class WallpapersLogicTest {
    @Rule public final ExpectedException exception = ExpectedException.none();
    @Rule public MockitoRule mockito = MockitoJUnit.rule();

    @Mock private PreferencesAdapter preferences ;
    @Mock private WallpapersAdapter adapter;
    @Mock private WallpapersRepository repository;
    private ArrayList<WallpaperModel> listOf3Wallpapers;
    private ArrayList<WallpaperModel> listOf0Wallpapers;
    private ArrayList<WallpaperModel> listOf1Wallpaper;
    private ArrayList<WallpaperModel> listOf3BasicWallpapers;

    @Before
    public void setUp() throws Exception {
        listOf3Wallpapers = new ArrayList<>();
        listOf3Wallpapers.add(new WallpaperModel(R.mipmap.fox01));
        listOf3Wallpapers.add(new WallpaperModel(R.mipmap.fox02));
        listOf3Wallpapers.add(new WallpaperModel(R.mipmap.fox03));

        listOf0Wallpapers = new ArrayList<>();

        listOf1Wallpaper = new ArrayList<>();
        listOf1Wallpaper.add(new WallpaperModel(R.mipmap.fox01));

        listOf3BasicWallpapers = new ArrayList<>();
        listOf3BasicWallpapers.add(new WallpaperModel(1));
        listOf3BasicWallpapers.add(new WallpaperModel(2));
        listOf3BasicWallpapers.add(new WallpaperModel(3));
    }

    @Test
    public void shouldNotAcceptNullRepository() throws Exception {
        exception.expect(IllegalArgumentException.class);
        WallpapersLogic wallpapers = new WallpapersLogic.Builder()
            .preferences(preferences)
            .adapter(adapter)
            .build();
    }

    @Test
    public void shouldNotAcceptNullPreferences() throws Exception {
        exception.expect(IllegalArgumentException.class);
        WallpapersLogic wallpapers = new WallpapersLogic.Builder()
            .repository(repository)
            .adapter(adapter)
            .build();
    }

    @Test
    public void shouldNotAcceptNullAdapter() throws Exception {
        exception.expect(IllegalArgumentException.class);
        WallpapersLogic wallpapers = new WallpapersLogic.Builder()
            .repository(repository)
            .preferences(preferences)
            .build();
    }

    @Test
    public void shouldLoad3Wallpapers() throws Exception {
        WallpapersLogic wallpapers = new WallpapersLogic.Builder()
            .preferences(preferences)
            .repository((WallpapersCallback caller) -> caller.wallpapersCallback(CallbackResult.Success, listOf3Wallpapers))
            .adapter(adapter)
            .build();

        assertEquals(3, wallpapers.count());
    }

    @Test
    public void setWallpaperShouldNotAcceptNegativeIndex() throws Exception {
        WallpapersLogic wallpapers = new WallpapersLogic.Builder()
            .preferences(preferences)
            .repository((WallpapersCallback caller) -> caller.wallpapersCallback(CallbackResult.Success, listOf1Wallpaper))
            .adapter(adapter)
            .build();

        exception.expect(IllegalArgumentException.class);
        wallpapers.setWallpaperWithIndex(-1);
    }

    @Test
    public void getWallpaperByIndex() throws Exception {
        WallpapersLogic wallpapers = new WallpapersLogic.Builder()
            .preferences(preferences)
            .repository((WallpapersCallback caller) -> caller.wallpapersCallback(CallbackResult.Success, listOf3BasicWallpapers))
            .adapter(adapter)
            .build();

        WallpaperModel wallpaper = wallpapers.getWallpaperByIndex(0);
        assertNotNull(wallpaper);
        assertEquals(1, wallpaper.getRid());

        wallpaper = wallpapers.getWallpaperByIndex(1);
        assertNotNull(wallpaper);
        assertEquals(2, wallpaper.getRid());

        wallpaper = wallpapers.getWallpaperByIndex(2);
        assertNotNull(wallpaper);
        assertEquals(3, wallpaper.getRid());
    }

    @Test
    public void getWallpaperByNegativeIndexShouldFail() throws Exception {
        WallpapersLogic wallpapers = new WallpapersLogic.Builder()
            .preferences(preferences)
            .repository((WallpapersCallback caller) -> caller.wallpapersCallback(CallbackResult.Success, listOf3BasicWallpapers))
            .adapter(adapter)
            .build();

        exception.expect(IllegalArgumentException.class);
        WallpaperModel wallpaper = wallpapers.getWallpaperByIndex(-2);
    }

    @Test
    public void getWallpaperByTooBigIndexShouldFail() throws Exception {
        WallpapersLogic wallpapers = new WallpapersLogic.Builder()
            .preferences(preferences)
            .repository((WallpapersCallback caller) -> caller.wallpapersCallback(CallbackResult.Success, listOf3BasicWallpapers))
            .adapter(adapter)
            .build();

        exception.expect(IllegalArgumentException.class);
        WallpaperModel wallpaper = wallpapers.getWallpaperByIndex(3);
    }

    @Test
    public void getLastSelectedWallpaperIndex() throws Exception {
        when(preferences.getLastSelectedWallpaperIndex())
            .thenReturn(1);

        WallpapersLogic wallpapers = new WallpapersLogic.Builder()
            .preferences(preferences)
            .repository((WallpapersCallback caller) -> caller.wallpapersCallback(CallbackResult.Success, listOf3BasicWallpapers))
            .adapter(adapter)
            .build();

        int index = wallpapers.getLastSelectedWallpaperIndex();
        assertEquals(1, index);
    }

    @Test
    public void saveLastSelectedWallpaperIndexFailsOnNegativeIndex() throws Exception {
        WallpapersLogic wallpapers = new WallpapersLogic.Builder()
            .preferences(preferences)
            .repository((WallpapersCallback caller) -> caller.wallpapersCallback(CallbackResult.Success, listOf1Wallpaper))
            .adapter(adapter)
            .build();

        exception.expect(IllegalArgumentException.class);
        wallpapers.saveLastSelectedWallpaperIndex(-1);
    }

    @Test
    public void saveLastSelectedWallpaperIndexFailsOnTooBigIndex() throws Exception {
        WallpapersLogic wallpapers = new WallpapersLogic.Builder()
            .preferences(preferences)
            .repository((WallpapersCallback caller) -> caller.wallpapersCallback(CallbackResult.Success, listOf3BasicWallpapers))
            .adapter(adapter)
            .build();

        exception.expect(IllegalArgumentException.class);
        wallpapers.saveLastSelectedWallpaperIndex(3);
    }

    @Test
    public void getLastSelectedWallpaperIndexShouldBeTheSameAsSavedLastSelectedWallpaperIndex() throws Exception {
        when(preferences.getLastSelectedWallpaperIndex())
            .thenReturn(2);

        WallpapersLogic wallpapers = new WallpapersLogic.Builder()
            .preferences(preferences)
            .repository((WallpapersCallback caller) -> caller.wallpapersCallback(CallbackResult.Success, listOf3BasicWallpapers))
            .adapter(adapter)
            .build();

        wallpapers.saveLastSelectedWallpaperIndex(2);
        int index = wallpapers.getLastSelectedWallpaperIndex();
        assertEquals(2, index);
    }

    @Test
    public void NoLoadedWallpaper() throws Exception {
        WallpapersLogic wallpapers = new WallpapersLogic.Builder()
            .preferences(preferences)
            .repository((WallpapersCallback caller) -> caller.wallpapersCallback(CallbackResult.Success, listOf0Wallpapers))
            .adapter(adapter)
            .build();

        assertEquals(0, wallpapers.count());
    }

    @Test
    public void getFixedImageRidArray() throws Exception {
        WallpapersLogic wallpapers = new WallpapersLogic.Builder()
            .preferences(preferences)
            .repository((WallpapersCallback caller) -> caller.wallpapersCallback(CallbackResult.Success, listOf3BasicWallpapers))
            .adapter(adapter)
            .build();

        int[] rids = wallpapers.getWallpaperImageRidArray();
        assertEquals(3, rids.length);

        assertEquals(1, rids[0]);
        assertEquals(2, rids[1]);
        assertEquals(3, rids[2]);
    }

    @Test
    public void getFixedImageRidStream() throws Exception {
        WallpapersLogic wallpapers = new WallpapersLogic.Builder()
            .preferences(preferences)
            .repository((WallpapersCallback caller) -> caller.wallpapersCallback(CallbackResult.Success, listOf3BasicWallpapers))
            .adapter(adapter)
            .build();

        int[] rids = wallpapers.getWallpaperImageRidStream().toArray();
        assertEquals(3, rids.length);

        assertEquals(1, rids[0]);
        assertEquals(2, rids[1]);
        assertEquals(3, rids[2]);
    }

    @Test
    public void getMocked3ImageRidArray() throws Exception {
        WallpapersLogic wallpapers = new WallpapersLogic.Builder()
            .preferences(preferences)
            .repository((WallpapersCallback caller) -> caller.wallpapersCallback(CallbackResult.Success, listOf3Wallpapers))
            .adapter(adapter)
            .build();

        int[] rids = wallpapers.getWallpaperImageRidArray();
        assertEquals(3, rids.length);

        assertEquals(R.mipmap.fox01, rids[0]);
        assertEquals(R.mipmap.fox02, rids[1]);
        assertEquals(R.mipmap.fox03, rids[2]);
    }

    @Test
    public void getMocked3ImageRidStram() throws Exception {
        WallpapersLogic wallpapers = new WallpapersLogic.Builder()
            .preferences(preferences)
            .repository((WallpapersCallback caller) -> caller.wallpapersCallback(CallbackResult.Success, listOf3Wallpapers))
            .adapter(adapter)
            .build();

        int[] rids = wallpapers.getWallpaperImageRidStream().toArray();
        assertEquals(3, rids.length);

        assertEquals(R.mipmap.fox01, rids[0]);
        assertEquals(R.mipmap.fox02, rids[1]);
        assertEquals(R.mipmap.fox03, rids[2]);
    }

    @Test
    public void setRandomWallpaperShouldPass() throws Exception {
        WallpapersLogic wallpapers = new WallpapersLogic.Builder()
            .preferences(preferences)
            .repository((WallpapersCallback caller) -> caller.wallpapersCallback(CallbackResult.Success, listOf3BasicWallpapers))
            .adapter(adapter)
            .build();

        wallpapers.setRandomWallpaper();
    }

    @Test
    public void setWallpaperWithIndexShouldPass() throws Exception {
        WallpapersLogic wallpapers = new WallpapersLogic.Builder()
            .preferences(preferences)
            .repository((WallpapersCallback caller) -> caller.wallpapersCallback(CallbackResult.Success, listOf3BasicWallpapers))
            .adapter(adapter)
            .build();

        wallpapers.setWallpaperWithIndex(1);
    }

    @Test
    public void saveLastSelectedWallpaperShouldCallPreferences() throws Exception {
        WallpapersLogic wallpapers = new WallpapersLogic.Builder()
            .preferences(preferences)
            .repository((WallpapersCallback caller) -> caller.wallpapersCallback(CallbackResult.Success, listOf3BasicWallpapers))
            .adapter(adapter)
            .build();

        wallpapers.saveLastSelectedWallpaperIndex(1);
        verify(preferences).saveLastSelectedWallpaperIndex(1);
    }

    @Test
    public void getLastSelectedWallpaperShouldCallPreferences() throws Exception {
        WallpapersLogic wallpapers = new WallpapersLogic.Builder()
            .preferences(preferences)
            .repository((WallpapersCallback caller) -> caller.wallpapersCallback(CallbackResult.Success, listOf3BasicWallpapers))
            .adapter(adapter)
            .build();

        wallpapers.getLastSelectedWallpaperIndex();
        verify(preferences).getLastSelectedWallpaperIndex();
    }

    @Test
    public void shouldObtainRandomWallpaperImages() throws Exception {
        WallpapersLogic wallpapers = new WallpapersLogic.Builder()
            .preferences(preferences)
            .repository((WallpapersCallback caller) -> caller.wallpapersCallback(CallbackResult.Success, listOf3Wallpapers))
            .adapter(adapter)
            .build();

        for (int i = 0; i < 10; i++) {
            WallpaperModel image1 = wallpapers.getRandomWallpaper();
            WallpaperModel image2 = wallpapers.getRandomWallpaper();
            WallpaperModel image3 = wallpapers.getRandomWallpaper();
            WallpaperModel image4 = wallpapers.getRandomWallpaper();
            WallpaperModel image5 = wallpapers.getRandomWallpaper();
            WallpaperModel image6 = wallpapers.getRandomWallpaper();

            assertTrue(listOf3Wallpapers.contains(image1));
            assertTrue(listOf3Wallpapers.contains(image2));
            assertTrue(listOf3Wallpapers.contains(image3));
            assertTrue(listOf3Wallpapers.contains(image4));
            assertTrue(listOf3Wallpapers.contains(image5));
            assertTrue(listOf3Wallpapers.contains(image6));

            assertTrue(image1.getRid() != image2.getRid()
                || image1.getRid() != image3.getRid()
                || image1.getRid() != image4.getRid()
                || image1.getRid() != image5.getRid()
                || image1.getRid() != image6.getRid()
            );
        }
    }
}