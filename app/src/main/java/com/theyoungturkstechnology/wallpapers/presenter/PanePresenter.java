package com.theyoungturkstechnology.wallpapers.presenter;

/**
 * Created by dvukotic on 26.01.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public interface PanePresenter {
    // life cycle
    void viewDidShow();
    void viewHasGone();
}
