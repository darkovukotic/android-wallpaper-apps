package com.theyoungturkstechnology.wallpapers.presenter;

import android.support.annotation.NonNull;

import com.theyoungturkstechnology.wallpapers.view.PaneType;
import com.theyoungturkstechnology.wallpapers.view.PaneView;


/**
 * Created by dvukotic on 16.01.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public class NavigationPaneContract {
    public interface View extends PaneView {
        void loadPane(@NonNull PaneType pane);
        void setBackgroundImage(int rid);
    }

    public interface Presenter extends PanePresenter {
        void onClickVersesPaneBtn();
        void onClickWallpapersPaneBtn();
        void onClickSettingsPaneBtn();
        void onAppActive();
        void onAppInactive();
    }
}

