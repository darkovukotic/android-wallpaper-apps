package com.theyoungturkstechnology.wallpapers.presenter;

import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.theyoungturkstechnology.wallpapers.repository.VersesCallback;
import com.theyoungturkstechnology.wallpapers.view.PaneView;

/**
 * Created by dvukotic on 16.01.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public class VersesPaneContract {
    public interface View extends PaneView {
        void disableReadBtn();
        void enableReadBtn();
        void setVersesText(@Nullable final String text, final float fontSize, final Typeface typeface);
        @Nullable String getVersesText();
    }

    public interface Presenter extends PanePresenter, VersesCallback {
        void onClickReadVersesBtn();
        void onClickNextVersesBtn();
        void onClickShareVersesBtn();
    }
}

