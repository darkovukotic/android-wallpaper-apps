package com.theyoungturkstechnology.wallpapers.presenter;

import android.support.annotation.Nullable;

import com.theyoungturkstechnology.wallpapers.exception.ForbiddenCodeException;
import com.theyoungturkstechnology.wallpapers.exception.NoObjectOrEmptyException;
import com.theyoungturkstechnology.wallpapers.model.VersesType;
import com.theyoungturkstechnology.wallpapers.repository.VersesRepository;
import com.theyoungturkstechnology.wallpapers.repository.VersesSource;
import com.theyoungturkstechnology.wallpapers.type.CallbackResult;
import com.theyoungturkstechnology.wallpapers.model.FontModel;
import com.theyoungturkstechnology.wallpapers.logic.FontsLogic;
import com.theyoungturkstechnology.wallpapers.model.LanguageModel;
import com.theyoungturkstechnology.wallpapers.logic.LanguagesLogic;
import com.theyoungturkstechnology.wallpapers.model.VersesModel;
import com.theyoungturkstechnology.wallpapers.presenter.VersesPaneContract.Presenter;
import com.theyoungturkstechnology.wallpapers.presenter.VersesPaneContract.View;
import com.theyoungturkstechnology.wallpapers.adapter.SpeakingAdapter;

import java.lang.ref.WeakReference;
import java.util.Locale;

import static com.theyoungturkstechnology.wallpapers.view.PaneView.ErrorLanguageNotSupportedByAndroid;
import static com.theyoungturkstechnology.wallpapers.view.PaneView.ErrorTryLaterMessage;

/**
 * Created by dvukotic on 25.12.2017.
 */

@SuppressWarnings("DefaultFileTemplate")
public class VersesPanePresenter implements Presenter, PanePresenter {
    private static final String TAG = VersesPanePresenter.class.getSimpleName() + " * ";
    @Nullable private FontsLogic fonts= null;
    @Nullable private SpeakingAdapter speaking= null;
    @Nullable private LanguagesLogic languages= null;
    @Nullable private VersesRepository versesRepository= null;
    private WeakReference<View> view = null;

    private VersesPanePresenter() {}

    private VersesPanePresenter(final WeakReference<View> _view,
                                @Nullable final FontsLogic _fonts,
                                @Nullable final SpeakingAdapter _speaking,
                                @Nullable final LanguagesLogic _languages,
                                @Nullable final VersesRepository _verses) {

        if (_view == null || _view.get() == null) {
            throw new IllegalArgumentException();
        }

        this.view = _view;
        this.fonts = _fonts;
        this.speaking = _speaking;
        this.languages = _languages;
        this.versesRepository = _verses;
    }

    public static class Builder {
        private FontsLogic fonts = null;
        private SpeakingAdapter speaking = null;
        private LanguagesLogic languages = null;
        private WeakReference<View> view = null;
        private VersesRepository verses = null;

        public Builder view(final View view) {
            this.view = new WeakReference<>(view);
            return this;
        }

        public Builder fonts(final FontsLogic fonts) {
            this.fonts = fonts;
            return this;
        }

        public Builder speaking(final SpeakingAdapter speaking) {
            this.speaking = speaking;
            return this;
        }

        public Builder languages(final LanguagesLogic languages) {
            this.languages = languages;
            return this;
        }

        public Builder verses(final VersesRepository verses) {
            this.verses = verses;
            return this;
        }

        public VersesPanePresenter build() {
            return new VersesPanePresenter(view, fonts, speaking, languages, verses);
        }
    }

    // PanePresenter

    @Override
    public void viewDidShow() {
        View viewObj = this.view.get();
        if (viewObj == null) {
            System.out.println("ERROR - We should have view here");
            return;
        }

        // enable/disable read button depending of if locale is supported or not
        if (languages == null || speaking == null) {
            viewObj.disableReadBtn();
        }
        else {
            Locale locale;

            try {
                locale = languages.getLocaleForCurrentLanguage();

                if (locale == null) {
                    System.out.println("ERROR - We should have locale here");
                    viewObj.disableReadBtn();
                }
                else {
                    speaking.isLocaleSupported(
                        locale,
                        () -> viewObj.enableReadBtn(),
                        () -> {
                            viewObj.disableReadBtn();
                            System.out.println("WARN - failed to use speaking service for locale " + locale.getDisplayName());
                            viewObj.showMessage(ErrorLanguageNotSupportedByAndroid);
                        }
                    );
                }
            }
            catch (Exception e) {
                e.printStackTrace();
                viewObj.disableReadBtn();
            }
        }

        // fetch and show verses
        if (versesRepository != null) {
            try { versesRepository.getVerses(VersesType.Random, this); }
            catch (Exception e) {
                e.printStackTrace();
                versesCallback(CallbackResult.Error, VersesSource.DB, VersesType.Random, null);
            }
        }
    }

    @Override
    public void viewHasGone() {
        // stop speaking
        if (speaking != null) speaking.stop();
    }

    // Presenter

    @Override
    public void onClickReadVersesBtn() {
        View viewObj = this.view.get();
        if (viewObj == null) {
            System.out.println("ERROR - We should have view here");
            return;
        }

        if (languages == null || speaking == null) {
            viewObj.disableReadBtn();
            return;
        }

        // button works as toggle
        if (speaking.isSpeaking()) {
            speaking.stop();
            return;
        }

        String text = getCurrentVersesString();
        if (text == null) {
            System.out.println("WARN - Nothing to read");
            viewObj.showMessage(ErrorTryLaterMessage);
            return;
        }

        Locale locale;

        try {
            locale = languages.getLocaleForCurrentLanguage();

            if (locale == null) {
                System.out.println("ERROR - We should have locale here");
                return;
            }

            if (text.isEmpty()) {
                System.out.println("WARN - Nothing to read");
                viewObj.showMessage(ErrorTryLaterMessage);
                return;
            }

            speaking.start(
                text,
                locale,
                ()->{
                    speaking.stop();
                    viewObj.disableReadBtn();
                    System.out.println("WARN - Error while using speaking service for locale " + locale.getDisplayName());
                    viewObj.showMessage(ErrorLanguageNotSupportedByAndroid);
                }
            );
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClickNextVersesBtn() {
        View viewObj = this.view.get();
        if (viewObj == null) {
            System.out.println("ERROR - We should have view here");
            return;
        }

        if (versesRepository != null) {
            try {
                versesRepository.getVerses(VersesType.Next, this);
            } catch (Exception e) {
                e.printStackTrace();
                versesCallback(CallbackResult.Error, VersesSource.DB, VersesType.Next, null);
            }
        }
    }

    @Override
    public void onClickShareVersesBtn() {
        // TODO
    }

    // Verses.Callback

    @Override
    public void versesCallback(CallbackResult result, VersesSource source, VersesType type, @Nullable VersesModel verses) {
        View viewObj = this.view.get();
        if (viewObj == null) {
            System.out.println("ERROR - We should have view here");
            return;
        }

        if (result != CallbackResult.Success || verses == null) {
            viewObj.showMessage(ErrorTryLaterMessage);
            return;
        }

        // because we shouldn't be here
        if (verses.getVersesStr() == null || verses.getVersesStr().isEmpty()) {
            throw new ForbiddenCodeException();
        }

        if (languages == null || fonts == null) {
            return;
        }

        LanguageModel language;

        try { language = languages.getCurrentLanguage(); }
        catch (Exception e) {
            e.printStackTrace();
            System.out.println("ERROR - We should have language here");
            return;
        }

        if (language == null) {
            System.out.println("ERROR - We should have language here");
            return;
        }

        switch (type) {
            case Next:
            case Random:
                FontModel font;

                try { font = fonts.getFontWithCode(language.getPreferredFontCode()); }
                catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("ERROR - We should have font here");
                    return;
                }

                if (font == null) {
                    System.out.println("ERROR - We should have font here");
                    return;
                }

                viewObj.setVersesText(
                    verses.getVersesStr(),
                    font.getPreferredSize(),
                    font.getTypeface()
                );
            break;

            case Current:
            default:
                throw new ForbiddenCodeException("Invalid enum Verses.Type");
        }
    }

    @Nullable private String getCurrentVersesString() {
        View viewObj = this.view.get();
        if (viewObj == null) {
            System.out.println("ERROR - We should have view here");
            return null;
        }

        return viewObj.getVersesText();
    }
}
