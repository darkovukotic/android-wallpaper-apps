package com.theyoungturkstechnology.wallpapers.presenter;

import android.annotation.TargetApi;

import com.theyoungturkstechnology.wallpapers.exception.ForbiddenCodeException;
import com.theyoungturkstechnology.wallpapers.logic.WallpapersLogic;
import com.theyoungturkstechnology.wallpapers.presenter.WallpapersPaneContract.*;
import com.theyoungturkstechnology.wallpapers.adapter.PreferencesAdapter;

import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.stream.IntStream;

import static com.theyoungturkstechnology.wallpapers.view.PaneView.ErrorTryLaterMessage;

/**
 * Created by dvukotic on 25.12.2017.
 */

@SuppressWarnings("DefaultFileTemplate")
public class WallpapersPanePresenter implements WallpapersPaneContract.Presenter, PanePresenter {
    private static final String TAG = WallpapersPanePresenter.class.getSimpleName() + " * ";
    private final PreferencesAdapter preferences;
    private final WallpapersLogic wallpapers;
    private final WeakReference<View> view;

    private WallpapersPanePresenter() {
        view = null;
        preferences = null;
        wallpapers = null;
    }

    private WallpapersPanePresenter(final WeakReference<View> _view, final WallpapersLogic _wallpapers, final PreferencesAdapter _preferences) {
        if (_view == null || _view.get() == null || _wallpapers == null || _preferences == null) {
            throw new IllegalArgumentException();
        }

        this.view = _view;
        this.wallpapers = _wallpapers;
        this.preferences = _preferences;
    }

    public static class Builder {
        private WallpapersLogic wallpapers = null;
        private PreferencesAdapter preferences = null;
        private WeakReference<View> view = null;

        public Builder view(final View view) {
            this.view = new WeakReference<>(view);
            return this;
        }

        public Builder wallpapers(final WallpapersLogic wallpapers) {
            this.wallpapers = wallpapers;
            return this;
        }

        public Builder preferences(final PreferencesAdapter preferences) {
            this.preferences = preferences;
            return this;
        }

        public WallpapersPanePresenter build() {
            return new WallpapersPanePresenter(view, wallpapers, preferences);
        }
    }

    // Presenter

    @Override
    public int[] getWallpaperImageRidArray() {
        try {
            return wallpapers.getWallpaperImageRidArray();
        } catch (Exception e) {
            e.printStackTrace();
            return new int[0];
        }
    }

    @TargetApi(24)
    @Override
    public IntStream getWallpaperImageRidStream() {
        try { return wallpapers.getWallpaperImageRidStream(); }
        catch (Exception e) {
            e.printStackTrace();
            return Arrays.stream(new int[0]);
        }
    }

    @Override
    public void selectedWallpaperWithIndex(int index) {
        try {
            // save selected to preferences
            wallpapers.saveLastSelectedWallpaperIndex(index);

            // set wallpaper
            wallpapers.setWallpaperWithIndex(index);

        } catch (Exception e) {
            System.out.println("In Unite Tests IGNORE this exception" );
            e.printStackTrace();

            View viewObj = this.view.get();
            if (viewObj == null) {
                throw new ForbiddenCodeException("We should have view here");
            }

            viewObj.showMessage(ErrorTryLaterMessage);
        }
    }

    // PanePresenter

    @Override
    public void viewDidShow() {
        // get last selected wallpaper index
        int index = wallpapers.getLastSelectedWallpaperIndex();

        // show it in flipper
        View viewObj = this.view.get();
        if (viewObj == null) {
            throw new ForbiddenCodeException("We should have view here");
        }

        viewObj.showInFlipperWallpaperWithIndex(index);
    }

    @Override
    public void viewHasGone() {
        // TODO
    }
}
