package com.theyoungturkstechnology.wallpapers.presenter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.theyoungturkstechnology.wallpapers.exception.ForbiddenCodeException;
import com.theyoungturkstechnology.wallpapers.model.WallpaperModel;
import com.theyoungturkstechnology.wallpapers.logic.WallpapersLogic;
import com.theyoungturkstechnology.wallpapers.presenter.NavigationPaneContract.Presenter;
import com.theyoungturkstechnology.wallpapers.presenter.NavigationPaneContract.View;
import com.theyoungturkstechnology.wallpapers.adapter.MusicAdapter;
import com.theyoungturkstechnology.wallpapers.adapter.PreferencesAdapter;
import com.theyoungturkstechnology.wallpapers.adapter.SpeakingAdapter;
import com.theyoungturkstechnology.wallpapers.view.PaneType;

import java.lang.ref.WeakReference;

/**
 * Created by dvukotic on 25.12.2017.
 */

@SuppressWarnings("DefaultFileTemplate")
public class NavigationPanePresenter implements Presenter, PanePresenter {
    private static final String TAG = NavigationPanePresenter.class.getSimpleName() + " * ";
    @Nullable private MusicAdapter music = null;
    @Nullable private WallpapersLogic wallpapers = null;
    @Nullable private PreferencesAdapter preferences = null;
    @Nullable private SpeakingAdapter speaking = null;
    private WeakReference<View> view = null;

    private NavigationPanePresenter() {}

    private NavigationPanePresenter(final WeakReference<View> _view,
                                    @Nullable final MusicAdapter _music,
                                    @Nullable final WallpapersLogic _wallpapers,
                                    @Nullable final PreferencesAdapter _preferences,
                                    @Nullable final SpeakingAdapter _speaking) {

        if (_view == null || _view.get() == null) {
            throw new IllegalArgumentException();
        }

        this.view = _view;
        this.music = _music;
        this.wallpapers = _wallpapers;
        this.preferences = _preferences;
        this.speaking = _speaking;
    }

    public static class Builder {
        private WeakReference<View> view = null;
        private MusicAdapter music = null;
        private WallpapersLogic wallpapers = null;
        private PreferencesAdapter preferences = null;
        private SpeakingAdapter speaking = null;

        public Builder view(final View view) {
            this.view = new WeakReference<>(view);
            return this;
        }

        public Builder music(final MusicAdapter music) {
            this.music = music;
            return this;
        }

        public Builder wallpapers(final WallpapersLogic wallpapers) {
            this.wallpapers = wallpapers;
            return this;
        }

        public Builder preferences(final PreferencesAdapter preferences) {
            this.preferences = preferences;
            return this;
        }

        public Builder speaking(final SpeakingAdapter speaking) {
            this.speaking = speaking;
            return this;
        }

        public NavigationPanePresenter build() {
            return new NavigationPanePresenter(view, music, wallpapers, preferences, speaking);
        }
    }

    // Presenter

    @Override
    public void onClickVersesPaneBtn() {
        View viewObj = this.view.get();
        if (viewObj == null) {
            System.out.println("ERROR - We should have view here");
            return;
        }

        viewObj.loadPane(PaneType.Verses);
    }

    @Override
    public void onClickWallpapersPaneBtn() {
        View viewObj = this.view.get();
        if (viewObj == null) {
            System.out.println("ERROR - We should have view here");
            return;
        }

        viewObj.loadPane(PaneType.Wallpapers);
    }

    @Override
    public void onClickSettingsPaneBtn() {
        View viewObj = this.view.get();
        if (viewObj == null) {
            System.out.println("ERROR - We should have view here");
            return;
        }

        viewObj.loadPane(PaneType.Settings);
    }

    @Override
    public void onAppActive() {
        View viewObj = this.view.get();
        if (viewObj == null) {
            System.out.println("ERROR - We should have view here");
            return;
        }

        // set background picture
        if (wallpapers != null) {
            try {
                WallpaperModel background = wallpapers.getRandomWallpaper();
                viewObj.setBackgroundImage(background.getRid());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        // start music if enabled
        if (music != null && preferences != null) {
            if (preferences.getIsMusicEnabled()) {
                music.play();
            }
        }
    }

    @Override
    public void onAppInactive() {
        // stop music
        if (music != null) {
            music.stop();
        }

        // stop speaking
        if (speaking != null) {
            speaking.stop();
        }
    }

    // PanePresenter

    @Override
    public void viewDidShow() {
        // TODO
    }

    @Override
    public void viewHasGone() {
        // TODO
    }


}
