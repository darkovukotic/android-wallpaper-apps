package com.theyoungturkstechnology.wallpapers.presenter;

import android.annotation.TargetApi;

import com.theyoungturkstechnology.wallpapers.view.PaneView;

import java.util.stream.IntStream;

/**
 * Created by dvukotic on 16.01.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
@TargetApi(24)
public class WallpapersPaneContract {
    public interface View extends PaneView {
        void showInFlipperWallpaperWithIndex(int index);
    }

    public interface Presenter extends PanePresenter {
        int[] getWallpaperImageRidArray();
        IntStream getWallpaperImageRidStream();
        void selectedWallpaperWithIndex(int index);
    }
}

