package com.theyoungturkstechnology.wallpapers.repository;

/**
 * Created by dvukotic on 06.03.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public interface FontsRepository {
    void getFonts(FontsCallback caller) throws Exception;
}
