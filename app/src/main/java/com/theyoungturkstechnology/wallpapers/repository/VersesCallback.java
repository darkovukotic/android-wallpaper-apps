package com.theyoungturkstechnology.wallpapers.repository;

import android.support.annotation.Nullable;

import com.theyoungturkstechnology.wallpapers.model.VersesModel;
import com.theyoungturkstechnology.wallpapers.model.VersesType;
import com.theyoungturkstechnology.wallpapers.type.CallbackResult;

/**
 * Created by dvukotic on 06.03.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public interface VersesCallback {
    void versesCallback(CallbackResult result, VersesSource source, VersesType type, @Nullable VersesModel verses);
}
