package com.theyoungturkstechnology.wallpapers.repository;

import com.theyoungturkstechnology.wallpapers.model.VersesType;

/**
 * Created by dvukotic on 09.03.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public interface VersesRepository {
    void getVerses(VersesType type, VersesCallback caller) throws Exception;
}
