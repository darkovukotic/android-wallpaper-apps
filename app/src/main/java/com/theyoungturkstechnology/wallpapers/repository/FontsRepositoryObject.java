package com.theyoungturkstechnology.wallpapers.repository;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;

import com.theyoungturkstechnology.wallpapers.MOCK.FontsRepositoryMock;
import com.theyoungturkstechnology.wallpapers.exception.ForbiddenCodeException;
import com.theyoungturkstechnology.wallpapers.exception.NoObjectOrEmptyException;
import com.theyoungturkstechnology.wallpapers.exception.NotImplementedException;
import com.theyoungturkstechnology.wallpapers.model.FontCode;
import com.theyoungturkstechnology.wallpapers.model.FontModel;
import com.theyoungturkstechnology.wallpapers.type.CallbackResult;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by dvukotic on 06.03.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
//public class FontsRepositoryObject implements FontsRepository {
//    private static final String TAG = FontsRepositoryObject.class.getSimpleName() + " * ";
//    private final Hashtable<String, Typeface> cache;
//    private WeakReference<Context> wContext;
//
//    private FontsRepositoryObject() {
//        this.wContext = null;
//        this.cache = null;
//    }
//
//    public FontsRepositoryObject(WeakReference<Context> _context) {
//        if (_context == null) {
//            throw new IllegalArgumentException();
//        }
//
//        this.wContext = _context;
//        this.cache = new Hashtable<>();
//    }
//
//    @Override
//    public void getFonts(FontsCallback caller) throws Exception {
//        throw new NotImplementedException();
//    }
//
//    @NonNull
//    private FontModel getFontForCode(FontCode code) throws Exception {
//        String assetPath = getAssetPathForCode(code);
//        Typeface typeface = getTypefaceForAssetPath(assetPath);
//        int preferredSize = getPreferredSizeForCode(code);
//
//        return new FontModel(code, typeface, preferredSize);
//    }
//
//    @NonNull
//    private String getAssetPathForCode(FontCode code) throws Exception {
//        throw new NotImplementedException();
//    }
//
//    private int getPreferredSizeForCode(FontCode code) throws Exception {
//        throw new NotImplementedException();
//    }
//
//    // assetPath example "fonts/EagleLake-Regular.ttf"
//    private Typeface getTypefaceForAssetPath(String assetPath) throws Exception {
//        if (assetPath == null || assetPath.isEmpty()) {
//            throw new IllegalArgumentException();
//        }
//
//        // cache it
//        synchronized (cache) {
//            if (cache.containsKey(assetPath)) {
//                return cache.get(assetPath);
//            }
//
//            Context context = wContext.get();
//            if (context == null) {
//                throw new ForbiddenCodeException();
//            }
//
//            Typeface typeface;
//
//            try {
//                typeface = Typeface.createFromAsset(context.getAssets(), assetPath);
//            } catch (Exception e) {
//                // not found
//                System.out.println(e.getMessage());
//                throw new NoObjectOrEmptyException();
//            }
//
//            if (typeface == null) {
//                throw new NoObjectOrEmptyException();
//            }
//
//            cache.put(assetPath, typeface);
//
//            return typeface;
//        }
//    }
//
//}

public class FontsRepositoryObject extends FontsRepositoryMock implements FontsRepository {

    public FontsRepositoryObject(WeakReference<Context> _context) {
        super(_context);
    }
}
