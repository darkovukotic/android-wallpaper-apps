package com.theyoungturkstechnology.wallpapers.repository;

import android.content.Context;
import android.graphics.Typeface;

import com.theyoungturkstechnology.wallpapers.MOCK.WallpapersRepositoryMock;
import com.theyoungturkstechnology.wallpapers.R;
import com.theyoungturkstechnology.wallpapers.exception.NotImplementedException;
import com.theyoungturkstechnology.wallpapers.model.WallpaperModel;
import com.theyoungturkstechnology.wallpapers.type.CallbackResult;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by dvukotic on 08.03.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
//public class WallpapersRepositoryObject implements WallpapersRepository {
//    private static final String TAG = WallpapersRepositoryObject.class.getSimpleName() + " * ";
//    private WeakReference<Context> wContext;
//
//    private WallpapersRepositoryObject() {
//        this.wContext = null;
//    }
//
//    public WallpapersRepositoryObject(WeakReference<Context> _context) {
//        if (_context == null) {
//            throw new IllegalArgumentException();
//        }
//
//        this.wContext = _context;
//    }
//
//    @Override
//    public void getWallpapers(WallpapersCallback caller) throws Exception {
//        throw new NotImplementedException();
//    }
//}

public class WallpapersRepositoryObject extends WallpapersRepositoryMock implements WallpapersRepository {
    public WallpapersRepositoryObject(WeakReference<Context> _context) {

    }
}
