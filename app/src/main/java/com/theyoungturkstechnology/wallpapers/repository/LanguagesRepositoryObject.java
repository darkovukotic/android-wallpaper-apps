package com.theyoungturkstechnology.wallpapers.repository;

import android.content.Context;

import com.theyoungturkstechnology.wallpapers.R;
import com.theyoungturkstechnology.wallpapers.exception.ForbiddenCodeException;
import com.theyoungturkstechnology.wallpapers.exception.NoObjectOrEmptyException;
import com.theyoungturkstechnology.wallpapers.model.FontCode;
import com.theyoungturkstechnology.wallpapers.model.LanguageModel;
import com.theyoungturkstechnology.wallpapers.type.CallbackResult;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dvukotic on 29.01.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public class LanguagesRepositoryObject implements LanguagesRepository {
    private static final String TAG = LanguagesRepositoryObject.class.getSimpleName() + " * ";
    private WeakReference<Context> wContext;

    private LanguagesRepositoryObject() {}

    public LanguagesRepositoryObject(WeakReference<Context> _context) {
        if (_context == null) {
            throw new IllegalArgumentException();
        }

        this.wContext = _context;
    }

    @Override
    public void getLanguages(LanguagesCallback caller) throws Exception {
        if (caller == null) {
            throw new IllegalArgumentException();
        }

        try {
            List<String> supportedCodes = getSupportedLanguageCodes();
            if (supportedCodes == null || supportedCodes.isEmpty()) {
                throw new NoObjectOrEmptyException();
            }

            List<FontCode> fontForSupportedCodes = getPreferredFontCodeForSupportedLanguageCodes();
            if (fontForSupportedCodes == null || fontForSupportedCodes.isEmpty() || fontForSupportedCodes.size() != supportedCodes.size()) {
                throw new NoObjectOrEmptyException();
            }

            String[] allLanguagesCodes = getAllLanguagesCodes();
            String[] allLanguagesNames = getAllLanguagesNames();
            if (allLanguagesCodes.length == 0
                || allLanguagesNames.length == 0
                || allLanguagesCodes.length != allLanguagesNames.length)
            {
                throw new ForbiddenCodeException();
            }

            ArrayList<LanguageModel> list = new ArrayList<>();

            for (int i = 0, index = 0; i < allLanguagesCodes.length && index < supportedCodes.size(); i++) {
                for (int j = 0; j < supportedCodes.size(); j++) {
                    if (allLanguagesCodes[i].compareToIgnoreCase(supportedCodes.get(j)) == 0) {
                        list.add(
                            new LanguageModel(
                                allLanguagesCodes[i],
                                allLanguagesNames[i],
                                fontForSupportedCodes.get(j)
                            )
                        );
                    }
                }
            }

            caller.languagesCallback(CallbackResult.Success, list);

        } catch (Exception e) {
            e.printStackTrace();
            caller.languagesCallback(CallbackResult.Error, new ArrayList<>());
        }
    }

    private List<String> getSupportedLanguageCodes() {
        // MOCK
        ArrayList<String> list = new ArrayList<>();
        list.add("en");
        list.add("sr");
        list.add("hr");
        list.add("bs");
        list.add("tr");
        list.add("ar");

        return list;
    }

    private List<FontCode> getPreferredFontCodeForSupportedLanguageCodes() {
        // MOCK
        ArrayList<FontCode> list = new ArrayList<>();
        list.add(FontCode.LatinFont);
        list.add(FontCode.LatinFont);
        list.add(FontCode.LatinFont);
        list.add(FontCode.LatinFont);
        list.add(FontCode.LatinFont);
        list.add(FontCode.ArabicFont);

        return list;
    }

    private String[] getAllLanguagesCodes() {
        Context context = wContext.get();
        if (context == null) {
            throw new ForbiddenCodeException();
        }

        return context.getResources().getStringArray(R.array.languageStringCode);
    }

    private String[] getAllLanguagesNames() {
        Context context = wContext.get();
        if (context == null) {
            throw new ForbiddenCodeException();
        }

        return context.getResources().getStringArray(R.array.languageNameOrig);
    }


    // checks just first character
//    public boolean stringIsRTL (final String str) {
//        if (str == null || str.isEmpty()) {
//            throw new IllegalArgumentException();
//        }
//
//        return Character.getDirectionality(str.charAt(0)) == Character.DIRECTIONALITY_RIGHT_TO_LEFT
//            || Character.getDirectionality(str.charAt(0)) == Character.DIRECTIONALITY_RIGHT_TO_LEFT_ARABIC
//            || Character.getDirectionality(str.charAt(0)) == Character.DIRECTIONALITY_RIGHT_TO_LEFT_EMBEDDING
//            || Character.getDirectionality(str.charAt(0)) == Character.DIRECTIONALITY_RIGHT_TO_LEFT_OVERRIDE;
//    }

}
