package com.theyoungturkstechnology.wallpapers.repository;

import com.theyoungturkstechnology.wallpapers.model.FontModel;
import com.theyoungturkstechnology.wallpapers.type.CallbackResult;

import java.util.List;

/**
 * Created by dvukotic on 06.03.2018.
 */

public interface FontsCallback {
    void fontsCallback(CallbackResult result, List<FontModel> fonts);
}
