package com.theyoungturkstechnology.wallpapers.repository;

import com.theyoungturkstechnology.wallpapers.adapter.DBAdapter;
import com.theyoungturkstechnology.wallpapers.adapter.ServerAdapter;
import com.theyoungturkstechnology.wallpapers.model.VersesModel;
import com.theyoungturkstechnology.wallpapers.model.VersesType;
import com.theyoungturkstechnology.wallpapers.tool.Tools;
import com.theyoungturkstechnology.wallpapers.type.CallbackResult;

import java.lang.ref.WeakReference;
import java.util.concurrent.Semaphore;


/**
 * Created by dvukotic on 12.02.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public class VersesRepositoryObject implements VersesRepository, VersesCallback {
    private static final String TAG = VersesRepositoryObject.class.getSimpleName() + " * ";
    private static Semaphore fetchSemaphore = new Semaphore(1);
    private VersesModel currentVerses;
    private final ServerAdapter server;
    private final DBAdapter db;
    private WeakReference<VersesCallback> wCaller;

    private VersesRepositoryObject() {
        this.server = null;
        this.db = null;
        this.currentVerses = VersesModel.Empty;
        this.wCaller = null;
    }

    private VersesRepositoryObject(final ServerAdapter _server, final DBAdapter _db) {
        if (_server == null || _db == null) {
            throw new IllegalArgumentException();
        }

        this.server = _server;
        this.db = _db;
        this.currentVerses = VersesModel.Empty;
        this.wCaller = null;
    }

    public static class Builder {
        private DBAdapter mDb = null;
        private ServerAdapter mServer = null;

        public Builder server(final ServerAdapter _server) {
            this.mServer = _server;
            return this;
        }

        public Builder database(final DBAdapter _db) {
            this.mDb = _db;
            return this;
        }

        public VersesRepositoryObject build() {
            return new VersesRepositoryObject(mServer, mDb);
        }
    }

    @Override
    public void getVerses(VersesType type, VersesCallback caller) throws Exception {
        wCaller = null;

        if (caller == null) {
            throw new IllegalArgumentException();
        }

        // we can always return current verses
        if (type == VersesType.Current) {
            getCurrentVerses(caller);
            return;
        }

        // if there is ongoing fetching drop new requests
        try {
            if (!fetchSemaphore.tryAcquire()) {
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
            fetchSemaphore.release();
        }

        wCaller = new WeakReference<>(caller);

        // first try from server
        getVersesFromServer(type);
    }

    private void getCurrentVerses(VersesCallback caller) throws Exception {
        if (currentVerses != null && !currentVerses.getVersesStr().isEmpty()) {
            caller.versesCallback(CallbackResult.Success, VersesSource.DB, VersesType.Current, currentVerses);
        } else {
            caller.versesCallback(CallbackResult.Error, VersesSource.DB, VersesType.Current, VersesModel.Empty);
        }
    }

    @Override
    public void versesCallback(CallbackResult result, VersesSource source, VersesType type, VersesModel verses) {
        VersesCallback caller = wCaller.get();
        if (caller == null) {
            System.out.println(TAG + ": " + "caller became null");
            fetchSemaphore.release();
            return;
        }

        // success
        if (result == CallbackResult.Success && verses != null && !verses.getVersesStr().isEmpty()) {
            // save
            currentVerses = verses;

            Tools.executeOnUiThread(()-> caller.versesCallback(result, source, type, verses) );

            fetchSemaphore.release();
            return;
        }

        // failure of network => try from db
        if (source == VersesSource.Server) {
            System.out.println(TAG + ": " + "network failed, trying db");
            getVersesFromDB(type);
            return;
        }

        // failure of db
        Tools.executeOnUiThread(()-> caller.versesCallback(CallbackResult.Error, source, type, VersesModel.Empty) );

        fetchSemaphore.release();
    }

    private void getVersesFromDB(VersesType type) {
        Tools.executeOnNewThread(()->{
            try { db.getVerses(type, this); }
            catch (Exception e) {
                e.printStackTrace();
                versesCallback(CallbackResult.Error, VersesSource.DB, type, VersesModel.Empty);
            }
        });
    }

    private void getVersesFromServer(VersesType type) {
        Tools.executeOnNewThread(()->{
            try { server.getVerses(type, this); }
            catch (Exception e) {
                e.printStackTrace();
                versesCallback(CallbackResult.Error, VersesSource.Server, type, VersesModel.Empty);
            }
        });
    }
}
