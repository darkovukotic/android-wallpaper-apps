package com.theyoungturkstechnology.wallpapers.repository;

import com.theyoungturkstechnology.wallpapers.model.FontModel;
import com.theyoungturkstechnology.wallpapers.model.LanguageModel;
import com.theyoungturkstechnology.wallpapers.type.CallbackResult;

import java.util.List;

/**
 * Created by dvukotic on 06.03.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public interface LanguagesCallback {
    void languagesCallback(CallbackResult result, List<LanguageModel> languages);
}
