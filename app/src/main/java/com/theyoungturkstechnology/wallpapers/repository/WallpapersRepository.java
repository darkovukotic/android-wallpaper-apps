package com.theyoungturkstechnology.wallpapers.repository;

/**
 * Created by dvukotic on 06.03.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public interface WallpapersRepository {
    void getWallpapers(WallpapersCallback caller) throws Exception;
}
