package com.theyoungturkstechnology.wallpapers.repository;

/**
 * Created by dvukotic on 29.01.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public interface LanguagesRepository {
    void getLanguages(LanguagesCallback caller) throws Exception;
}
