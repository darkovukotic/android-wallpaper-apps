package com.theyoungturkstechnology.wallpapers.repository;

import com.theyoungturkstechnology.wallpapers.model.FontModel;
import com.theyoungturkstechnology.wallpapers.model.WallpaperModel;
import com.theyoungturkstechnology.wallpapers.type.CallbackResult;

import java.util.List;

/**
 * Created by dvukotic on 06.03.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public interface WallpapersCallback {
    void wallpapersCallback(CallbackResult result, List<WallpaperModel> wallpapers);
}
