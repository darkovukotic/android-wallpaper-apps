package com.theyoungturkstechnology.wallpapers.exception;

/**
 * Created by dvukotic on 22.12.2017.
 */

@SuppressWarnings("DefaultFileTemplate")
public class NoObjectOrEmptyException extends RuntimeException {
    public NoObjectOrEmptyException(String message) {
        super("No object or empty: " + message);
    }

    public NoObjectOrEmptyException() {
        super("No object or empty");
    }
}
