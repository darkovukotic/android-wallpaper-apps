package com.theyoungturkstechnology.wallpapers.exception;

/**
 * Created by dvukotic on 22.12.2017.
 */

@SuppressWarnings("DefaultFileTemplate")
public class ForbiddenCodeException extends RuntimeException {
    public ForbiddenCodeException(String message) {
        super("We shouldn't be here: " + message);
    }

    public ForbiddenCodeException() {
        super("We shouldn't be here");
    }
}
