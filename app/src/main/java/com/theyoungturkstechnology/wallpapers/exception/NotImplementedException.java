package com.theyoungturkstechnology.wallpapers.exception;

/**
 * Created by dvukotic on 22.12.2017.
 */

@SuppressWarnings("DefaultFileTemplate")
public class NotImplementedException extends RuntimeException {
    public NotImplementedException(String message) {
        super("Not implemented: " + message);
    }

    public NotImplementedException() {
        super("Not implemented");
    }
}
