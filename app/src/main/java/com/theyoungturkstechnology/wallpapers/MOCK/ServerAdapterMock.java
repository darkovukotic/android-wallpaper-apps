package com.theyoungturkstechnology.wallpapers.MOCK;

import com.theyoungturkstechnology.wallpapers.adapter.ServerAdapter;
import com.theyoungturkstechnology.wallpapers.model.VersesModel;
import com.theyoungturkstechnology.wallpapers.model.VersesType;
import com.theyoungturkstechnology.wallpapers.repository.VersesCallback;
import com.theyoungturkstechnology.wallpapers.repository.VersesSource;
import com.theyoungturkstechnology.wallpapers.type.CallbackResult;

/**
 * Created by dvukotic on 12.03.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public class ServerAdapterMock implements ServerAdapter {
    @Override
    public void getVerses(VersesType type, VersesCallback caller) throws Exception {
        if (caller == null) {
            throw new IllegalArgumentException();
        }

        VersesModel model = type == VersesType.Random ?
            new VersesModel("Random verses from server 1 Random verses from server 2 Random verses from server 3 Random verses from server 4")
            : new VersesModel("Next verses from server 1 Next verses from server 2 Next verses from server 3 Next verses from server 4");

        caller.versesCallback(CallbackResult.Success, VersesSource.Server, type, model);
    }
}
