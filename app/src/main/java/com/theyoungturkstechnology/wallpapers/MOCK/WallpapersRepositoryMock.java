package com.theyoungturkstechnology.wallpapers.MOCK;

import com.theyoungturkstechnology.wallpapers.R;
import com.theyoungturkstechnology.wallpapers.model.FontCode;
import com.theyoungturkstechnology.wallpapers.model.LanguageModel;
import com.theyoungturkstechnology.wallpapers.model.WallpaperModel;
import com.theyoungturkstechnology.wallpapers.repository.WallpapersCallback;
import com.theyoungturkstechnology.wallpapers.repository.WallpapersRepository;
import com.theyoungturkstechnology.wallpapers.type.CallbackResult;

import java.util.ArrayList;

/**
 * Created by dvukotic on 08.03.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public class WallpapersRepositoryMock implements WallpapersRepository {
    @Override
    public void getWallpapers(WallpapersCallback caller) throws Exception {
        if (caller == null) {
            throw new IllegalArgumentException();
        }

        ArrayList<WallpaperModel> list = new ArrayList<>();
        list.add(new WallpaperModel(R.mipmap.fox01));
        list.add(new WallpaperModel(R.mipmap.fox02));
        list.add(new WallpaperModel(R.mipmap.fox03));

        caller.wallpapersCallback(CallbackResult.Success, list);
    }
}
