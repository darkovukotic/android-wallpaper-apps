package com.theyoungturkstechnology.wallpapers.MOCK;

import com.theyoungturkstechnology.wallpapers.adapter.PreferencesAdapter;

/**
 * Created by dvukotic on 08.03.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public class PreferencesAdapterMock implements PreferencesAdapter {
    private int lastSelectedWallpaperIndex = 0;
    private boolean isMusicEnabled = true;
    private String currentLanguageCode = "";

    @Override
    public int getLastSelectedWallpaperIndex() {
        return lastSelectedWallpaperIndex;
    }

    @Override
    public void saveLastSelectedWallpaperIndex(int index) {
        if (index < 0) {
            throw new IllegalArgumentException();
        }

        lastSelectedWallpaperIndex = index;
    }

    @Override
    public boolean getIsMusicEnabled() {
        return isMusicEnabled;
    }

    @Override
    public void saveIsMusicEnabled(boolean enabled) {
        isMusicEnabled = enabled;
    }

    @Override
    public String getCurrentLanguageCode() {
        return currentLanguageCode;
    }

    @Override
    public void saveCurrentLanguageCode(String code) {
        if (code == null || code.isEmpty()) {
            throw new IllegalArgumentException();
        }

        currentLanguageCode = code;
    }
}
