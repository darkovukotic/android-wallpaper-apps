package com.theyoungturkstechnology.wallpapers.MOCK;

import com.theyoungturkstechnology.wallpapers.adapter.DBAdapter;
import com.theyoungturkstechnology.wallpapers.model.VersesModel;
import com.theyoungturkstechnology.wallpapers.model.VersesType;
import com.theyoungturkstechnology.wallpapers.repository.VersesCallback;
import com.theyoungturkstechnology.wallpapers.repository.VersesSource;
import com.theyoungturkstechnology.wallpapers.type.CallbackResult;
import com.theyoungturkstechnology.wallpapers.type.CallbackWithResult;

/**
 * Created by dvukotic on 12.03.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public class DBAdapterMock implements DBAdapter {
    @Override
    public void getVerses(VersesType type, VersesCallback caller) throws Exception {
        if (caller == null) {
            throw new IllegalArgumentException();
        }

        VersesModel model = type == VersesType.Random ?
            new VersesModel("Random verses from DB 1 Random verses from DB 2 Random verses from DB 3 Random verses from DB 4")
            : new VersesModel("Next verses from DB 1 Next verses from DB 2 Next verses from DB 3 Next verses from DB 4");

        caller.versesCallback(CallbackResult.Success, VersesSource.DB, type, model);
    }

    @Override
    public void saveVerses(CallbackWithResult caller) throws Exception {
        if (caller == null) {
            throw new IllegalArgumentException();
        }

        caller.callbackWithResult(CallbackResult.Success);
    }
}
