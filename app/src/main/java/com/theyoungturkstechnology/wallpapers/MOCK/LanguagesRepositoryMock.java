package com.theyoungturkstechnology.wallpapers.MOCK;

import com.theyoungturkstechnology.wallpapers.model.FontCode;
import com.theyoungturkstechnology.wallpapers.model.LanguageModel;
import com.theyoungturkstechnology.wallpapers.repository.LanguagesCallback;
import com.theyoungturkstechnology.wallpapers.repository.LanguagesRepository;
import com.theyoungturkstechnology.wallpapers.type.CallbackResult;

import java.util.ArrayList;

/**
 * Created by dvukotic on 08.03.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public class LanguagesRepositoryMock implements LanguagesRepository {
    @Override
    public void getLanguages(LanguagesCallback caller) throws Exception {
        if (caller == null) {
            throw new IllegalArgumentException();
        }

        ArrayList<LanguageModel> list = new ArrayList<>();
        list.add(new LanguageModel("en", "English", FontCode.LatinFont));
        list.add(new LanguageModel("sr", "Srpski", FontCode.LatinFont));
        list.add(new LanguageModel("hr", "Hrvatski", FontCode.LatinFont));
        list.add(new LanguageModel("bs", "Bosnjacki", FontCode.LatinFont));
        list.add(new LanguageModel("tr", "Türkçe", FontCode.LatinFont));
        list.add(new LanguageModel("ar", "Arabic", FontCode.ArabicFont));

        caller.languagesCallback(CallbackResult.Success, list);
    }
}
