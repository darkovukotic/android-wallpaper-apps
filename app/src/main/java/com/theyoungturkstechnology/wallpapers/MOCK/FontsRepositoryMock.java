package com.theyoungturkstechnology.wallpapers.MOCK;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;

import com.theyoungturkstechnology.wallpapers.exception.ForbiddenCodeException;
import com.theyoungturkstechnology.wallpapers.exception.NoObjectOrEmptyException;
import com.theyoungturkstechnology.wallpapers.model.FontCode;
import com.theyoungturkstechnology.wallpapers.model.FontModel;
import com.theyoungturkstechnology.wallpapers.repository.FontsCallback;
import com.theyoungturkstechnology.wallpapers.repository.FontsRepository;
import com.theyoungturkstechnology.wallpapers.type.CallbackResult;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by dvukotic on 06.03.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public class FontsRepositoryMock implements FontsRepository {
    private static final String TAG = FontsRepositoryMock.class.getSimpleName() + " * ";
    private final Hashtable<String, Typeface> cache;
    private WeakReference<Context> wContext;

    private FontsRepositoryMock() {
        this.wContext = null;
        this.cache = null;
    }

    public FontsRepositoryMock(WeakReference<Context> _context) {
        if (_context == null) {
            throw new IllegalArgumentException();
        }

        this.wContext = _context;
        this.cache = new Hashtable<>();
    }

    @Override
    public void getFonts(FontsCallback caller) throws Exception {
        if (caller == null) {
            throw new IllegalArgumentException();
        }

        try {
            // MOCK
            ArrayList<FontModel> list = new ArrayList<>();
            list.add(getFontForCode(FontCode.ArabicFont));
            list.add(getFontForCode(FontCode.CyrillicFont));
            list.add(getFontForCode(FontCode.HindiFont));
            list.add(getFontForCode(FontCode.LatinFont));

            caller.fontsCallback(CallbackResult.Success, list);

        } catch (Exception e) {
            e.printStackTrace();
            caller.fontsCallback(CallbackResult.Error, new ArrayList<>());
        }
    }

    @NonNull
    private FontModel getFontForCode(FontCode code) throws Exception {
        String assetPath = getAssetPathForCode(code);
        Typeface typeface = getTypefaceForAssetPath(assetPath);
        int preferredSize = getPreferredSizeForCode(code);

        return new FontModel(code, typeface, preferredSize);
    }

    @NonNull
    private String getAssetPathForCode(FontCode code) throws Exception {
        // MOCK
        String prefix = "fonts/";
        String suffix = ".ttf";

        switch (code) {
            case LatinFont:
                return prefix + "EagleLake-Regular" + suffix;

            case HindiFont:
                return prefix + "Himalaya" + suffix;

            case CyrillicFont:
                return prefix + "Mirosln" + suffix;

            case ArabicFont:
                return prefix + "PDMS_Saleem_QuranFont" + suffix;

            default:
                System.out.println("Invalid font code: " + code);
                throw new ForbiddenCodeException();
        }
    }

    private int getPreferredSizeForCode(FontCode code) throws Exception {
        // MOCK
        switch (code) {
            case LatinFont:
                return 20;

            case HindiFont:
                return 27;

            case CyrillicFont:
                return 24;

            case ArabicFont:
                return 25;

            default:
                System.out.println("Invalid font code: " + code);
                throw new ForbiddenCodeException();
        }
    }

    // assetPath example "fonts/EagleLake-Regular.ttf"
    private Typeface getTypefaceForAssetPath(String assetPath) throws Exception {
        if (assetPath == null || assetPath.isEmpty()) {
            throw new IllegalArgumentException();
        }

        // cache it
        synchronized (cache) {
            if (cache.containsKey(assetPath)) {
                return cache.get(assetPath);
            }

            Context context = wContext.get();
            if (context == null) {
                throw new ForbiddenCodeException();
            }

            Typeface typeface;

            try {
                typeface = Typeface.createFromAsset(context.getAssets(), assetPath);
            } catch (Exception e) {
                // not found
                System.out.println(e.getMessage());
                throw new NoObjectOrEmptyException();
            }

            if (typeface == null) {
                throw new NoObjectOrEmptyException();
            }

            cache.put(assetPath, typeface);

            return typeface;
        }
    }

}
