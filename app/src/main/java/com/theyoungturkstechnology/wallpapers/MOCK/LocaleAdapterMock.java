package com.theyoungturkstechnology.wallpapers.MOCK;

import com.theyoungturkstechnology.wallpapers.adapter.LocaleAdapter;

import java.util.Locale;

/**
 * Created by dvukotic on 08.03.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public class LocaleAdapterMock implements LocaleAdapter {
    @Override
    public Locale getLocaleForLanguageCode(String code) throws Exception {
        return Locale.US;
    }

    @Override
    public String getPhoneLanguageCode() throws Exception {
        return "en";
    }
}
