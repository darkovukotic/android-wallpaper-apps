package com.theyoungturkstechnology.wallpapers.logic;

import android.annotation.TargetApi;

import com.theyoungturkstechnology.wallpapers.exception.ForbiddenCodeException;
import com.theyoungturkstechnology.wallpapers.exception.NoObjectOrEmptyException;
import com.theyoungturkstechnology.wallpapers.model.FontModel;
import com.theyoungturkstechnology.wallpapers.model.WallpaperModel;
import com.theyoungturkstechnology.wallpapers.repository.WallpapersRepository;
import com.theyoungturkstechnology.wallpapers.tool.Tools;
import com.theyoungturkstechnology.wallpapers.adapter.PreferencesAdapter;
import com.theyoungturkstechnology.wallpapers.adapter.WallpapersAdapter;
import com.theyoungturkstechnology.wallpapers.type.CallbackResult;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.stream.IntStream;


/**
 * Created by dvukotic on 22.12.2017.
 */

@SuppressWarnings("DefaultFileTemplate")
public class WallpapersLogic {
    private static final String TAG = WallpapersLogic.class.getSimpleName() + " * ";
    private List<WallpaperModel> wallpapers;
    private final WallpapersRepository repository;
    private final WallpapersAdapter adapter;
    private final PreferencesAdapter preferences;
    private int previousRandomWallpaperIndex = -1;
    private static Semaphore loadSemaphore = new Semaphore(1);

    private WallpapersLogic() {
        this.adapter = null;
        this.preferences = null;
        this.wallpapers = null;
        this.repository = null;
    }

    private WallpapersLogic(final WallpapersAdapter _wallpaperAdapter, final PreferencesAdapter _preferences, final WallpapersRepository _repository) {
        if (_wallpaperAdapter == null || _preferences == null || _repository == null) {
            throw new IllegalArgumentException();
        }

        this.adapter = _wallpaperAdapter;
        this.preferences = _preferences;
        this.repository = _repository;

        loadWallpapers();
    }

    public static class Builder {
        private WallpapersAdapter mAdapter = null;
        private PreferencesAdapter mPreferences = null;
        private WallpapersRepository mRepository = null;

        public Builder adapter(final WallpapersAdapter _adapter) {
            this.mAdapter = _adapter;
            return this;
        }

        public Builder repository(final WallpapersRepository _repository) {
            this.mRepository = _repository;
            return this;
        }

        public Builder preferences(final PreferencesAdapter _preferences) {
            this.mPreferences = _preferences;
            return this;
        }

        public WallpapersLogic build() {
            return new WallpapersLogic(mAdapter, mPreferences, mRepository);
        }
    }

    public int count() {
        // block just during load
        try {
            loadSemaphore.acquire();
            loadSemaphore.release();
        } catch (Exception e) {
            e.printStackTrace();
            loadSemaphore.release();
        }

        return wallpapers != null ? wallpapers.size() : 0;
    }

    private void loadWallpapers() {
        try {
            loadSemaphore.acquire();
        } catch (Exception e) {
            e.printStackTrace();
            loadSemaphore.release();
        }

        Tools.executeOnNewThread(()->{
            try {
                repository.getWallpapers(
                    (CallbackResult _result, List<WallpaperModel> _wallpapers) -> {
                        if (_result == CallbackResult.Success) {
                            wallpapers = _wallpapers;
                        }

                        loadSemaphore.release();
                    }
                );
            }
            catch (Exception e) {
                e.printStackTrace();
                loadSemaphore.release();
            }
        });
    }

    public WallpaperModel getRandomWallpaper() throws Exception {
        // block just during load
        try {
            loadSemaphore.acquire();
            loadSemaphore.release();
        } catch (Exception e) {
            e.printStackTrace();
            loadSemaphore.release();
        }

        if (count() == 0) {
            throw new NoObjectOrEmptyException();
        }

        int rnd = Tools.getRandomPositiveInt();
        int index = rnd % count();

        if (previousRandomWallpaperIndex == index) {
            // try one more time
            rnd = Tools.getRandomPositiveInt();
            index = rnd % count();

            if (previousRandomWallpaperIndex == index) {
                // and one more time
                rnd = Tools.getRandomPositiveInt();
                index = rnd % count();
            }
        }

        previousRandomWallpaperIndex = index;

        return getWallpaperByIndex(index);
    }

    public void setRandomWallpaper() throws Exception {
        // block just during load
        try {
            loadSemaphore.acquire();
            loadSemaphore.release();
        } catch (Exception e) {
            e.printStackTrace();
            loadSemaphore.release();
        }

        if (count() == 0) {
            throw new NoObjectOrEmptyException();
        }

        int rnd = Tools.getRandomPositiveInt();
        int index = rnd % count();

        setWallpaperWithIndex(index);
    }

    public void setWallpaperWithIndex(int index) throws Exception {
        // block just during load
        try {
            loadSemaphore.acquire();
            loadSemaphore.release();
        } catch (Exception e) {
            e.printStackTrace();
            loadSemaphore.release();
        }

        if (count() == 0) {
            throw new NoObjectOrEmptyException();
        }

        if (index >= count() || index < 0) {
            throw new IllegalArgumentException();
        }

        WallpaperModel wallpaper = getWallpaperByIndex(index);

        Tools.executeOnNewThread(()->{
            try {
                adapter.setWallpaper(wallpaper);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

    }

    public WallpaperModel getWallpaperByIndex(int index) throws Exception {
        // block just during load
        try {
            loadSemaphore.acquire();
            loadSemaphore.release();
        } catch (Exception e) {
            e.printStackTrace();
            loadSemaphore.release();
        }

        if (count() == 0) {
            throw new NoObjectOrEmptyException();
        }

        if (index >= count() || index < 0) {
            throw new IllegalArgumentException();
        }

        return wallpapers.get(index);
    }

    public int getLastSelectedWallpaperIndex() {
        // block just during load
        try {
            loadSemaphore.acquire();
            loadSemaphore.release();
        } catch (Exception e) {
            e.printStackTrace();
            loadSemaphore.release();
        }

        if (count() == 0) {
            throw new NoObjectOrEmptyException();
        }

        int index = preferences.getLastSelectedWallpaperIndex();

        // maybe app version has changed
        if (index >= count()) {
            index = 0;
            preferences.saveLastSelectedWallpaperIndex(index);
        }

        if (index < 0) {
            throw new ForbiddenCodeException("Invalid index value");
        }

        return index;
    }

    public void saveLastSelectedWallpaperIndex(int index) throws Exception {
        // block just during load
        try {
            loadSemaphore.acquire();
            loadSemaphore.release();
        } catch (Exception e) {
            e.printStackTrace();
            loadSemaphore.release();
        }

        if (count() == 0) {
            throw new NoObjectOrEmptyException();
        }

        if (index >= count() || index < 0) {
            throw new IllegalArgumentException();
        }

        preferences.saveLastSelectedWallpaperIndex(index);
    }

    public int[] getWallpaperImageRidArray() throws Exception {
        // block just during load
        try {
            loadSemaphore.acquire();
            loadSemaphore.release();
        } catch (Exception e) {
            e.printStackTrace();
            loadSemaphore.release();
        }

        if (count() == 0) {
            throw new NoObjectOrEmptyException();
        }

        int[] rids = new int[count()];
        int i = 0;

        for (WallpaperModel wallpaper : wallpapers) {
            rids[i++] = wallpaper.getRid();
        }

        return rids;
    }

    @TargetApi(24)
    public IntStream getWallpaperImageRidStream() throws Exception {
        // block just during load
        try {
            loadSemaphore.acquire();
            loadSemaphore.release();
        } catch (Exception e) {
            e.printStackTrace();
            loadSemaphore.release();
        }

        if (count() == 0) {
            throw new NoObjectOrEmptyException();
        }

        return wallpapers
            .stream()
            .mapToInt(WallpaperModel::getRid);
    }
}
