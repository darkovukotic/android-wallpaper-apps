package com.theyoungturkstechnology.wallpapers.logic;

import android.annotation.TargetApi;
import android.os.Build;

import com.theyoungturkstechnology.wallpapers.exception.NoObjectOrEmptyException;
import com.theyoungturkstechnology.wallpapers.model.FontCode;
import com.theyoungturkstechnology.wallpapers.model.FontModel;
import com.theyoungturkstechnology.wallpapers.repository.FontsRepository;
import com.theyoungturkstechnology.wallpapers.tool.Tools;
import com.theyoungturkstechnology.wallpapers.type.CallbackResult;

import java.util.List;
import java.util.concurrent.Semaphore;

/**
 * Created by dvukotic on 06.02.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public class FontsLogic {
    private static final String TAG = FontsLogic.class.getSimpleName() + " * ";
    private final FontsRepository repository;
    private List<FontModel> fonts;
    private static Semaphore loadSemaphore = new Semaphore(1);

    private FontsLogic() {
        this.repository = null;
        this.fonts = null;
    }

    private FontsLogic(final FontsRepository _repository) {
        if (_repository == null) {
            throw new IllegalArgumentException();
        }

        this.repository = _repository;
        this.fonts = null;
        loadFonts();
    }

    public static class Builder {
        private FontsRepository mRepository;

        public Builder repository(final FontsRepository _repository) {
            this.mRepository = _repository;
            return this;
        }

        public FontsLogic build() {
            return new FontsLogic(mRepository);
        }
    }

    public int count() {
        // block just during load
        try {
            loadSemaphore.acquire();
            loadSemaphore.release();
        } catch (Exception e) {
            e.printStackTrace();
            loadSemaphore.release();
        }

        return fonts != null ? fonts.size() : 0;
    }

    private void loadFonts() {
        try {
            loadSemaphore.acquire();
        } catch (Exception e) {
            e.printStackTrace();
            loadSemaphore.release();
        }

        Tools.executeOnNewThread(()->{
            try {
                repository.getFonts(
                    (CallbackResult _result, List<FontModel> _fonts) -> {
                        if (_result == CallbackResult.Success) {
                            fonts = _fonts;
                        }

                        loadSemaphore.release();
                    }
                );
            }
            catch (Exception e) {
                e.printStackTrace();
                loadSemaphore.release();
            }
        });
    }

    @TargetApi(24)
    public FontModel getFontWithCode(FontCode code) throws Exception {
        // block just during load
        try {
            loadSemaphore.acquire();
            loadSemaphore.release();
        } catch (Exception e) {
            e.printStackTrace();
            loadSemaphore.release();
        }

        if (!(count() > 0)) {
            throw new NoObjectOrEmptyException();
        }

        if (Build.VERSION.SDK_INT >= 24) {
            try {
                return fonts
                    .stream()
                    .filter(f -> f.getCode() == code)
                    .findFirst()
                    .get();

            } catch (Exception e) {
                e.printStackTrace();
                throw new NoObjectOrEmptyException();
            }
        }
        // old android versions
        else {
            for (FontModel f: fonts) {
                if (f.getCode() == code) {
                    return f;
                }
            }

            throw new NoObjectOrEmptyException();
        }
    }

}
