package com.theyoungturkstechnology.wallpapers.logic;

import com.theyoungturkstechnology.wallpapers.exception.ForbiddenCodeException;
import com.theyoungturkstechnology.wallpapers.exception.NoObjectOrEmptyException;
import com.theyoungturkstechnology.wallpapers.model.FontModel;
import com.theyoungturkstechnology.wallpapers.model.LanguageModel;
import com.theyoungturkstechnology.wallpapers.repository.LanguagesRepository;
import com.theyoungturkstechnology.wallpapers.adapter.LocaleAdapter;
import com.theyoungturkstechnology.wallpapers.adapter.PreferencesAdapter;
import com.theyoungturkstechnology.wallpapers.tool.Tools;
import com.theyoungturkstechnology.wallpapers.type.CallbackResult;

import java.util.List;
import java.util.Locale;
import java.util.concurrent.Semaphore;

/**
 * Created by dvukotic on 29.01.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public class LanguagesLogic {
    private static final String TAG = LanguagesLogic.class.getSimpleName() + " * ";
    private final LanguagesRepository repository;
    private final PreferencesAdapter preferences;
    private final LocaleAdapter localeAdapter;
    private List<LanguageModel> languages;
    private static Semaphore loadSemaphore = new Semaphore(1);

    private LanguagesLogic() {
        this.repository = null;
        this.preferences = null;
        this.localeAdapter = null;
        this.languages = null;
    }

    private LanguagesLogic(final LanguagesRepository _repository, final PreferencesAdapter _preferences, final LocaleAdapter _localeAdapter) {
        if (_repository == null || _preferences == null || _localeAdapter == null) {
            throw new IllegalArgumentException();
        }

        this.repository = _repository;
        this.preferences = _preferences;
        this.localeAdapter = _localeAdapter;
        this.languages = null;
        loadLanguages();
    }

    public static class Builder {
        private LanguagesRepository mRepository = null;
        private PreferencesAdapter mPreferences = null;
        private LocaleAdapter mLocaleAdapter = null;

        public Builder repository(final LanguagesRepository _repository) {
            this.mRepository = _repository;
            return this;
        }

        public Builder preferences(final PreferencesAdapter _preferences) {
            this.mPreferences = _preferences;
            return this;
        }

        public Builder locale(final LocaleAdapter _localeAdapter) {
            this.mLocaleAdapter = _localeAdapter;
            return this;
        }

        public LanguagesLogic build() {
            return new LanguagesLogic(mRepository, mPreferences, mLocaleAdapter);
        }
    }

    public int count() {
        // block just during load
        try {
            loadSemaphore.acquire();
            loadSemaphore.release();
        } catch (Exception e) {
            e.printStackTrace();
            loadSemaphore.release();
        }

        return languages != null ? languages.size() : 0;
    }

    private void loadLanguages() {
        try {
            loadSemaphore.acquire();
        } catch (Exception e) {
            e.printStackTrace();
            loadSemaphore.release();
        }

        Tools.executeOnNewThread(()->{
            try {
                repository.getLanguages(
                    (CallbackResult _result, List<LanguageModel> _languages) -> {
                        if (_result == CallbackResult.Success) {
                            languages = _languages;
                        }

                        loadSemaphore.release();
                    }
                );
            }
            catch (Exception e) {
                e.printStackTrace();
                loadSemaphore.release();
            }
        });
    }

    public List<LanguageModel> getLanguages() {
        // block just during load
        try {
            loadSemaphore.acquire();
            loadSemaphore.release();
        } catch (Exception e) {
            e.printStackTrace();
            loadSemaphore.release();
        }

        if (!(count() > 0)) {
            throw new NoObjectOrEmptyException();
        }

        return languages;
    }

    public Locale getLocaleForLanguage(LanguageModel language) throws Exception {
        if (language == null) {
            throw new IllegalArgumentException();
        }

        return localeAdapter.getLocaleForLanguageCode(language.getCode());
    }

    public Locale getLocaleForCurrentLanguage() throws Exception {
        LanguageModel language = getCurrentLanguage();
        if (language == null) {
            throw new NoObjectOrEmptyException();
        }

        return localeAdapter.getLocaleForLanguageCode(language.getCode());
    }

    public LanguageModel getCurrentLanguage() throws Exception {
        String code = preferences.getCurrentLanguageCode();
        if (code == null) {
            throw new ForbiddenCodeException();
        }

        LanguageModel currentLanguage;

        // if it's default (invalid) language then get phone's language
        if (code.isEmpty()) {
            String phoneLanguageCode = localeAdapter.getPhoneLanguageCode();

            if (phoneLanguageCode != null) {
                // do we support phone's language?
                LanguageModel phoneLanguage = getLanguageForCode(phoneLanguageCode);

                if (phoneLanguage != null) {
                    // save it to properties
                    saveCurrentLanguage(phoneLanguage);
                    return phoneLanguage;
                }
            }

            // if we don't support it use english
            currentLanguage = getLanguageForCode("en");
            // and save it to properties
            saveCurrentLanguage(currentLanguage);

        } else {
            currentLanguage = getLanguageForCode(code);
        }

        return currentLanguage;
    }

    public void saveCurrentLanguage(LanguageModel language) {
        if (language == null) {
            throw new IllegalArgumentException();
        }

        preferences.saveCurrentLanguageCode(language.getCode());
    }

    private LanguageModel getLanguageForCode(String code) throws Exception {
        if (code == null || code.isEmpty()) {
            throw new IllegalArgumentException();
        }

        for (LanguageModel lang : languages) {
            if (lang.getCode().compareToIgnoreCase(code) == 0) {
                return lang;
            }
        }

        throw new NoObjectOrEmptyException();
    }
}
