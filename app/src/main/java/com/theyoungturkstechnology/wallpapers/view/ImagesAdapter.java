package com.theyoungturkstechnology.wallpapers.view;

import android.content.Context;
import android.support.annotation.VisibleForTesting;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.theyoungturkstechnology.wallpapers.R;

/**
 * Created by Darko on 22/01/2017.
 */
@SuppressWarnings("DefaultFileTemplate")
class ImagesAdapter extends BaseAdapter {
    private int[] imageRids;
    private LayoutInflater inflater;

    ImagesAdapter(Context _applicationContext, int[] _imageRids) {
        if (_applicationContext == null || _imageRids == null) {
            throw new IllegalArgumentException();
        }

        this.imageRids = _imageRids;
        inflater = LayoutInflater.from(_applicationContext);
    }

    @Override
    public int getCount() {
        return imageRids.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.image_list_item, parent, false);
        }

        ImageView imageView = convertView.findViewById(R.id.imageView);
        imageView.setImageResource(imageRids[position]);
        return convertView;
    }
}
