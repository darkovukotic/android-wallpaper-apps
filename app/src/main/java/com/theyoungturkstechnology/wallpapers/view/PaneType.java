package com.theyoungturkstechnology.wallpapers.view;

/**
 * Created by dvukotic on 23.03.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public enum PaneType {
    Settings,
    Wallpapers,
    Verses,
    About
}
