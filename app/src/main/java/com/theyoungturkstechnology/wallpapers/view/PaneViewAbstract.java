package com.theyoungturkstechnology.wallpapers.view;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import java.lang.ref.WeakReference;

/**
 * Created by dvukotic on 09.01.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
abstract class PaneViewAbstract implements PaneView, Pane {
    protected WeakReference<MainActivity> activity;
    protected ConstraintLayout paneRootView;

    protected PaneViewAbstract(){}

    PaneViewAbstract(WeakReference<MainActivity> _activity, int _paneRootViewRid) {
        MainActivity mActivity = _activity.get();
        if (mActivity == null) {
            throw new IllegalArgumentException();
        }

        this.activity = _activity;
        this.paneRootView = mActivity.findViewById(_paneRootViewRid);

        if (this.paneRootView == null) {
            throw new IllegalArgumentException();
        }
    }

    // Pane

    @Override
    public void load() {
        paneRootView.setVisibility(View.VISIBLE);
    }

    @Override
    public void unload() {
        paneRootView.setVisibility(View.GONE);
    }

    @Override
    public boolean onTouchEvent(MotionEvent touchEvent) {
        return false;
    }

    // PaneView

    @Override
    public void showMessage(int messageRid) {
        Activity activityObj = this.activity.get();
        if (activityObj == null) {
            System.out.println("ERROR - We should have activity here");
            return;
        }

        Toast
            .makeText(
                activityObj,
                activityObj
                    .getResources()
                    .getString(messageRid),
                Toast.LENGTH_LONG
            )
            .show();
    }

}
