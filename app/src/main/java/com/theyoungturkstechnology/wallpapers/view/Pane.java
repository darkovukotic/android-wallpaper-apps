package com.theyoungturkstechnology.wallpapers.view;

import android.view.MotionEvent;

/**
 * Created by dvukotic on 23.03.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public interface Pane {
    // life cycle
    void load();
    void unload();

    // ui events
    boolean onTouchEvent(MotionEvent touchEvent);
}
