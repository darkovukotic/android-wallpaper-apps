package com.theyoungturkstechnology.wallpapers.view;

import android.app.Activity;
import android.graphics.Rect;
import android.os.Build;
import android.support.annotation.NonNull;
import android.view.MotionEvent;
import android.widget.AdapterViewFlipper;

import com.theyoungturkstechnology.wallpapers.R;
import com.theyoungturkstechnology.wallpapers.exception.ForbiddenCodeException;
import com.theyoungturkstechnology.wallpapers.presenter.PanePresenter;
import com.theyoungturkstechnology.wallpapers.presenter.WallpapersPaneContract.*;

import java.lang.ref.WeakReference;


/**
 * Created by dvukotic on 05.01.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public class WallpapersPaneView extends PaneViewAbstract implements View  {
    private Presenter presenter = null;
    private AdapterViewFlipper imagesFlipper = null;
    private float initialSwipeX;
    private boolean isSwipingImagesFlipper;

    public WallpapersPaneView(WeakReference<MainActivity> _activity, int _paneRootViewRid) {
        super(_activity, _paneRootViewRid);

        if (_activity == null || _paneRootViewRid < 0) {
            throw new IllegalArgumentException();
        }

        // set wallpaper btn
        paneRootView.findViewById(R.id.setWallpaperBtn).setOnClickListener(
            view -> {
                if (presenter == null) {
                    throw new ForbiddenCodeException("We should have presenter here");
                }
                int index = imagesFlipper.getDisplayedChild();
                presenter.selectedWallpaperWithIndex(index);
            }
        );

        // previous wallpaper btn
        paneRootView.findViewById(R.id.previousWallpaperBtn).setOnClickListener(
            view -> imagesFlipper.showPrevious()
        );

        // next wallpaper btn
        paneRootView.findViewById(R.id.nextWallpaperBtn).setOnClickListener(
            view -> imagesFlipper.showNext()
        );
    }

    // PaneView

    @Override
    public void load() {
        super.load();

        if (imagesFlipper == null) {
            Activity activityObj = this.activity.get();
            if (activityObj == null) {
                throw new ForbiddenCodeException("We should have activity here");
            }

            // wallpaper image flipper
            int[] imageRids;

            if (Build.VERSION.SDK_INT < 24) {
                imageRids = presenter.getWallpaperImageRidArray();
            } else {
                imageRids = presenter
                    .getWallpaperImageRidStream()
                    .toArray();
            }

            ImagesAdapter imagesAdapter = new ImagesAdapter(activityObj.getApplicationContext(), imageRids);
            imagesFlipper = activityObj.findViewById(R.id.flipperView);
            imagesFlipper.setAdapter(imagesAdapter);
        }

        if (presenter == null) {
            throw new ForbiddenCodeException("We should have presenter here");
        }

        presenter.viewDidShow();
    }

    @Override
    public void unload() {
        if (presenter == null) {
            throw new ForbiddenCodeException("We should have presenter here");
        }

        presenter.viewHasGone();

        super.unload();
    }

    @Override
    public void setPresenter(@NonNull PanePresenter _presenter) {
        this.presenter = (Presenter)_presenter;
    }

    public boolean onTouchEvent(MotionEvent touchEvent) {
        switch (touchEvent.getAction()) {
            case MotionEvent.ACTION_DOWN:
                initialSwipeX = touchEvent.getX();
                float y = touchEvent.getY();

                Rect rect = new Rect(
                    imagesFlipper.getLeft(),
                    imagesFlipper.getTop(),
                    imagesFlipper.getLeft() + imagesFlipper.getWidth(),
                    imagesFlipper.getTop() + imagesFlipper.getHeight());

                isSwipingImagesFlipper = rect.contains((int)initialSwipeX, (int)y);
                break;

            case MotionEvent.ACTION_UP:
                float finalSwipeX = touchEvent.getX();
                boolean isSwipeLeft = initialSwipeX > finalSwipeX;
                boolean isSwipe = Math.abs(initialSwipeX - finalSwipeX) > imagesFlipper.getWidth() / 10.0;

                if (!isSwipe) {
                    break;
                }

                if (!isSwipingImagesFlipper) {
                    break;
                }
                isSwipingImagesFlipper = false;

                if (isSwipeLeft) {
                    imagesFlipper.showNext();
                } else {
                    imagesFlipper.showPrevious();
                }
                break;
        }

        // not consumed
        return super.onTouchEvent(touchEvent);
    }

    // View

    @Override
    public void showInFlipperWallpaperWithIndex(int index) {
        if (index < 0) {
            throw new IllegalArgumentException();
        }

        imagesFlipper.setDisplayedChild(index);
    }
}
