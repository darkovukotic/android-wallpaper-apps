package com.theyoungturkstechnology.wallpapers.view;

import android.support.annotation.NonNull;

import com.theyoungturkstechnology.wallpapers.R;
import com.theyoungturkstechnology.wallpapers.presenter.PanePresenter;

/**
 * Created by dvukotic on 05.01.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public interface PaneView {
    // messages
    int ErrorTryLaterMessage = R.string.errorTryLater;
    int ErrorLanguageNotSupportedByAndroid = R.string.errorLanguageNotSupportedByAndroid;
    void showMessage(int messageRid);

    // mvp
    void setPresenter(@NonNull PanePresenter presenter);
}
