package com.theyoungturkstechnology.wallpapers.view;

import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.TransitionManager;
import android.view.MotionEvent;

import com.theyoungturkstechnology.wallpapers.R;
import com.theyoungturkstechnology.wallpapers.exception.NotImplementedException;
import com.theyoungturkstechnology.wallpapers.provider.Provider;

@SuppressWarnings("DefaultFileTemplate")
public class MainActivity extends AppCompatActivity {
    @Nullable private Pane wallpapersPane = null;
    @Nullable private Pane versesPane = null;
    @Nullable private NavigationPaneView navigationPane = null;
    @Nullable private Pane currentPane = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // provider does the magic
        Provider provider = Provider.instance();
        provider.setAppContext(this.getApplicationContext());
        navigationPane = provider.provideNavigationPaneView(this, R.id.navigationPaneRootView);
        navigationPane.load();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (navigationPane == null) {
            System.out.println("ERROR - we should have navigationPane");
            return;
        }
        navigationPane.onAppActive();
    }

    @Override
    protected void onPause() {
        if (navigationPane == null) {
            System.out.println("ERROR - we should have navigationPane");
        } else {
            navigationPane.onAppInactive();
        }
        super.onPause();
    }

    @Override
    public boolean onTouchEvent(MotionEvent touchEvent) {
        if (currentPane == null) {
            // not consumed
            return false;
        }

        return currentPane.onTouchEvent(touchEvent);
    }

    public void loadPane(PaneType paneType) {
        Provider provider = Provider.instance();
        Pane newPane = null;

        switch (paneType) {
            case About:
                throw new NotImplementedException();

            case Settings:
                throw new NotImplementedException();

            case Wallpapers:
                if (wallpapersPane == null) {
                    wallpapersPane = provider.provideWallpapersPaneView(this, R.id.wallpapersPaneRootView);
                }
                newPane = wallpapersPane;
            break;

            case Verses:
                if (versesPane == null) {
                    versesPane = provider.provideVersesPaneView(this, R.id.versesPaneRootView);
                }
                newPane = versesPane;
            break;
        }

        if (newPane != currentPane) {
            if (Build.VERSION.SDK_INT >= 19) {
                TransitionManager.beginDelayedTransition(findViewById(R.id.rootView));
            }

            if (currentPane != null) {
                currentPane.unload();
            }

            currentPane = newPane;
            currentPane.load();
        }
    }
}
