package com.theyoungturkstechnology.wallpapers.view;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.Button;

import com.theyoungturkstechnology.wallpapers.R;
import com.theyoungturkstechnology.wallpapers.presenter.PanePresenter;
import com.theyoungturkstechnology.wallpapers.presenter.NavigationPaneContract.Presenter;
import com.theyoungturkstechnology.wallpapers.presenter.NavigationPaneContract.View;

import java.lang.ref.WeakReference;


/**
 * Created by dvukotic on 05.01.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public class NavigationPaneView extends PaneViewAbstract implements View  {
    @Nullable private Presenter presenter = null;
    @Nullable private Button versesPaneBtn = null;
    @Nullable private Button wallpapersPaneBtn = null;
    @Nullable private Button settingsPaneBtn = null;

    private NavigationPaneView() {}

    public NavigationPaneView(WeakReference<MainActivity> _activity, int paneRootViewRid) {
        super(_activity, paneRootViewRid);

        // verses pane btn
        versesPaneBtn = paneRootView.findViewById(R.id.versesPaneBtn);
        if (versesPaneBtn != null) {
            versesPaneBtn.setOnClickListener(
                view -> {
                    if (presenter == null) {
                        System.out.println("ERROR - We should have presenter here");
                        return;
                    }
                    presenter.onClickVersesPaneBtn();
                }
            );
        }

        // wallpapers pane btn
        wallpapersPaneBtn = paneRootView.findViewById(R.id.wallpapersPaneBtn);
        if (wallpapersPaneBtn != null) {
            wallpapersPaneBtn.setOnClickListener(
                view -> {
                    if (presenter == null) {
                        System.out.println("ERROR - We should have presenter here");
                        return;
                    }
                    presenter.onClickWallpapersPaneBtn();
                }
            );
        }

        // settings pane btn
        settingsPaneBtn = paneRootView.findViewById(R.id.settingsPaneBtn);
        if (settingsPaneBtn != null) {
            settingsPaneBtn.setOnClickListener(
                view -> {
                    if (presenter == null) {
                        System.out.println("ERROR - We should have presenter here");
                        return;
                    }
                    presenter.onClickSettingsPaneBtn();
                }
            );
        }
    }

    // PaneView

    @Override
    public void setPresenter(@NonNull PanePresenter _presenter) {
        this.presenter = (Presenter) _presenter;
    }

    // Pane

    @Override
    public void load() {
        super.load();

        if (presenter == null) {
            System.out.println("ERROR - We should have presenter here");
            return;
        }

        presenter.viewDidShow();
    }

    @Override
    public void unload() {
        if (presenter == null) {
            System.out.println("ERROR - We should have presenter here");
            return;
        }

        presenter.viewHasGone();

        super.unload();
    }

    // View

    public void loadPane(@NonNull PaneType pane) {
        MainActivity activityObj = this.activity.get();
        if (activityObj == null) {
            System.out.println("ERROR - We should have activity here");
            return;
        }

        activityObj.loadPane(pane);
    }

    @Override
    public void setBackgroundImage(int rid) {
        Activity activityObj = this.activity.get();
        if (activityObj == null) {
            System.out.println("ERROR - We should have activity here");
            return;
        }

        activityObj.findViewById(R.id.rootView).setBackgroundResource(rid);
    }

    public void onAppActive() {
        if (presenter == null) {
            System.out.println("ERROR - We should have presenter here");
            return;
        }

        presenter.onAppActive();
    }

    public void onAppInactive() {
        if (presenter == null) {
            System.out.println("ERROR - We should have presenter here");
            return;
        }

        presenter.onAppInactive();
    }
}
