package com.theyoungturkstechnology.wallpapers.view;

import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.TypedValue;
import android.view.*;
import android.widget.Button;
import android.widget.TextView;

import com.theyoungturkstechnology.wallpapers.R;
import com.theyoungturkstechnology.wallpapers.exception.ForbiddenCodeException;
import com.theyoungturkstechnology.wallpapers.presenter.PanePresenter;
import com.theyoungturkstechnology.wallpapers.presenter.VersesPaneContract.*;
import com.theyoungturkstechnology.wallpapers.presenter.VersesPaneContract.View;

import java.lang.ref.WeakReference;

import static android.view.View.TEXT_DIRECTION_ANY_RTL;

/**
 * Created by dvukotic on 07.02.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public class VersesPaneView extends PaneViewAbstract implements View {
    @Nullable private Presenter presenter = null;
    @Nullable private TextView versesTextView =  null;
    @Nullable private Button readVersesBtn = null;

    private VersesPaneView() {}

    public VersesPaneView(WeakReference<MainActivity> _activity, int _paneRootViewRid) {
        super(_activity, _paneRootViewRid);

        versesTextView = paneRootView.findViewById(R.id.verses);

        // read verses btn
        readVersesBtn = paneRootView.findViewById(R.id.readVersesBtn);
        if (readVersesBtn != null) {
            readVersesBtn.setOnClickListener(
                view -> {
                    if (presenter == null) {
                        System.out.println("ERROR - We should have presenter here");
                        return;
                    }
                    presenter.onClickReadVersesBtn();
                }
            );
        }

        // share verses btn
        Button shareVersesBtn = paneRootView.findViewById(R.id.shareVersesBtn);
        if (shareVersesBtn != null) {
            shareVersesBtn.setOnClickListener(
                view -> {
                    if (presenter == null) {
                        System.out.println("ERROR - We should have presenter here");
                        return;
                    }
                    presenter.onClickShareVersesBtn();
                }
            );
        }

        // next btn
        Button nextVersesBtn = paneRootView.findViewById(R.id.nextVersesBtn);
        if (nextVersesBtn != null) {
            nextVersesBtn.setOnClickListener(
                view -> {
                    if (presenter == null) {
                        System.out.println("ERROR - We should have presenter here");
                        return;
                    }
                    presenter.onClickNextVersesBtn();
                }
            );
        }
    }

    // Pane

    @Override
    public void load() {
        super.load();

        if (presenter == null) {
            System.out.println("ERROR - We should have presenter here");
            return;
        }

        presenter.viewDidShow();
    }

    @Override
    public void unload() {
        if (presenter == null) {
            System.out.println("ERROR - We should have presenter here");
            return;
        }

        presenter.viewHasGone();

        super.unload();
    }

    @Override
    public boolean onTouchEvent(MotionEvent touchEvent) {
        return super.onTouchEvent(touchEvent);
    }

    // PaneView

    @Override
    public void setPresenter(@NonNull PanePresenter _presenter) {
        this.presenter = (Presenter)_presenter;
    }

    // View

    @Override
    public void disableReadBtn() {
        if (readVersesBtn != null) readVersesBtn.setEnabled(false);
    }

    @Override
    public void enableReadBtn() {
        if (readVersesBtn != null) readVersesBtn.setEnabled(true);
    }

    @Override
    public void setVersesText(@Nullable final String text, final float fontSize, final Typeface typeface) {
        if (text == null) {
            System.out.println("WARN - Received null text to show");
            return;
        }

        if (fontSize < 0 || typeface == null) {
            throw new IllegalArgumentException();
        }

        if (versesTextView == null) {
            System.out.println("ERROR - We should have versesTextView");
            return;
        }

        versesTextView.setTextDirection(TEXT_DIRECTION_ANY_RTL);
        versesTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, fontSize);
        versesTextView.setTypeface(typeface);
        versesTextView.setText(text);
        versesTextView.scrollTo(0, 0);
    }

    @Override
    @Nullable public String getVersesText() {
        if (versesTextView == null) {
            System.out.println("ERROR - We should have versesTextView");
            return null;
        }

        if (versesTextView.getText().toString().isEmpty()) return null;

        return versesTextView.getText().toString();
    }
}
