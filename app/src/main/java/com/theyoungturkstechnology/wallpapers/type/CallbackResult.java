package com.theyoungturkstechnology.wallpapers.type;

/**
 * Created by dvukotic on 06.03.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public enum CallbackResult {
    Error,
    Success
}
