package com.theyoungturkstechnology.wallpapers.type;

import com.theyoungturkstechnology.wallpapers.model.FontModel;

import java.util.List;

/**
 * Created by dvukotic on 06.03.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public interface Callback {
    void callback();
}
