package com.theyoungturkstechnology.wallpapers.provider;

import android.content.Context;

import com.theyoungturkstechnology.wallpapers.adapter.DBAdapter;
import com.theyoungturkstechnology.wallpapers.adapter.DBAdapterObject;
import com.theyoungturkstechnology.wallpapers.adapter.ServerAdapter;
import com.theyoungturkstechnology.wallpapers.adapter.ServerAdapterObject;
import com.theyoungturkstechnology.wallpapers.logic.FontsLogic;
import com.theyoungturkstechnology.wallpapers.logic.LanguagesLogic;
import com.theyoungturkstechnology.wallpapers.repository.VersesRepository;
import com.theyoungturkstechnology.wallpapers.repository.VersesRepositoryObject;
import com.theyoungturkstechnology.wallpapers.logic.WallpapersLogic;
import com.theyoungturkstechnology.wallpapers.presenter.NavigationPaneContract;
import com.theyoungturkstechnology.wallpapers.presenter.NavigationPanePresenter;
import com.theyoungturkstechnology.wallpapers.presenter.VersesPaneContract;
import com.theyoungturkstechnology.wallpapers.presenter.VersesPanePresenter;
import com.theyoungturkstechnology.wallpapers.presenter.WallpapersPaneContract;
import com.theyoungturkstechnology.wallpapers.presenter.WallpapersPanePresenter;
import com.theyoungturkstechnology.wallpapers.repository.FontsRepository;
import com.theyoungturkstechnology.wallpapers.repository.FontsRepositoryObject;
import com.theyoungturkstechnology.wallpapers.repository.LanguagesRepositoryObject;
import com.theyoungturkstechnology.wallpapers.repository.LanguagesRepository;
import com.theyoungturkstechnology.wallpapers.adapter.LocaleAdapterObject;
import com.theyoungturkstechnology.wallpapers.adapter.LocaleAdapter;
import com.theyoungturkstechnology.wallpapers.adapter.MusicAdapter;
import com.theyoungturkstechnology.wallpapers.adapter.MusicAdapterObject;
import com.theyoungturkstechnology.wallpapers.adapter.PreferencesAdapter;
import com.theyoungturkstechnology.wallpapers.adapter.PreferencesAdapterObject;
import com.theyoungturkstechnology.wallpapers.adapter.SpeakingAdapter;
import com.theyoungturkstechnology.wallpapers.adapter.SpeakingAdapterObject;
import com.theyoungturkstechnology.wallpapers.adapter.WallpapersAdapterObject;
import com.theyoungturkstechnology.wallpapers.adapter.WallpapersAdapter;
import com.theyoungturkstechnology.wallpapers.repository.WallpapersRepository;
import com.theyoungturkstechnology.wallpapers.repository.WallpapersRepositoryObject;
import com.theyoungturkstechnology.wallpapers.view.MainActivity;
import com.theyoungturkstechnology.wallpapers.view.NavigationPaneView;
import com.theyoungturkstechnology.wallpapers.view.VersesPaneView;
import com.theyoungturkstechnology.wallpapers.view.WallpapersPaneView;

import java.lang.ref.WeakReference;

/**
 * Created by Darko on 25.01.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public class Provider {
    private static volatile Provider instance;
    private WeakReference<Context> appContext = null;
    // adapters
    private PreferencesAdapter preferencesAdapter = null;
    private WallpapersAdapter wallpapersAdapter = null;
    private MusicAdapter musicAdapter = null;
    private LocaleAdapter localeAdapter = null;
    private SpeakingAdapter speakingAdapter = null;
    private DBAdapter dbAdapter = null;
    private ServerAdapter serverAdapter = null;
    // repositories
    private LanguagesRepository languagesRepository = null;
    private FontsRepository fontsRepository = null;
    private WallpapersRepository wallpapersRepository = null;
    private VersesRepository versesRepository = null;
    // models
    private WallpapersLogic wallpapersLogic = null;
    private LanguagesLogic languagesLogic = null;
    private FontsLogic fontsLogic = null;

    private Provider() {}

    public static Provider instance() {
        if (instance == null) {
            synchronized (Provider.class) {
                if (instance == null) {
                    instance = new Provider();
                }
            }
        }

        return instance;
    }

    public void setAppContext(Context context) {
        if (context == null) {
            throw new IllegalArgumentException();
        }

        appContext = new WeakReference<>(context);
    }

    // wallpapers pane
    public WallpapersPaneView provideWallpapersPaneView(MainActivity activity, int paneRootViewRid) {
        WeakReference<MainActivity> wActivity = new WeakReference<>(activity);

        WallpapersPaneView view = new WallpapersPaneView(wActivity, paneRootViewRid);
        // form presenter too
        view.setPresenter(provideWallpapersPanePresenter(view));
        return view;
    }

    public WallpapersPaneContract.Presenter provideWallpapersPanePresenter(WallpapersPaneContract.View view) {
        return new WallpapersPanePresenter.Builder()
            .view(view)
            .wallpapers(provideWallpapersLogic())
            .preferences(providePreferencesAdapter())
            .build();
    }

    // verses pane
    public VersesPaneView provideVersesPaneView(MainActivity activity, int paneRootViewRid) {
        WeakReference<MainActivity> wActivity = new WeakReference<>(activity);

        VersesPaneView view = new VersesPaneView(wActivity, paneRootViewRid);
        // form presenter too
        view.setPresenter(provideVersesPanePresenter(view));
        return view;
    }

    public VersesPaneContract.Presenter provideVersesPanePresenter(VersesPaneContract.View view) {
        return new VersesPanePresenter.Builder()
            .view(view)
            .fonts(provideFontsLogic())
            .languages(provideLanguagesLogic())
            .speaking(provideSpeakingAdapter())
            .verses(provideVersesRepository())
            .build();
    }

    // navigation pane
    public NavigationPaneView provideNavigationPaneView(MainActivity activity, int paneRootViewRid) {
        WeakReference<MainActivity> wActivity = new WeakReference<>(activity);

        NavigationPaneView view = new NavigationPaneView(wActivity, paneRootViewRid);
        // form presenter too
        view.setPresenter(provideNavigationPanePresenter(view));
        return view;
    }

    public NavigationPaneContract.Presenter provideNavigationPanePresenter(NavigationPaneContract.View view) {
        return new NavigationPanePresenter.Builder()
            .view(view)
            .wallpapers(provideWallpapersLogic())
            .music(provideMusicAdapter())
            .preferences(providePreferencesAdapter())
            .speaking(provideSpeakingAdapter())
            .build();
    }

    // models (no context)
    public FontsLogic provideFontsLogic() {
        if (fontsLogic == null) {
            fontsLogic = new FontsLogic.Builder()
                .repository(provideFontsRepository())
                .build();
        }

        return fontsLogic;
    }

    public LanguagesLogic provideLanguagesLogic() {
        if (languagesLogic == null) {
            languagesLogic = new LanguagesLogic.Builder()
                .repository(provideLanguagesRepository())
                .preferences(providePreferencesAdapter())
                .locale(provideLocaleAdapter())
                .build();
        }

        return languagesLogic;
    }

    public WallpapersLogic provideWallpapersLogic() {
        if (wallpapersLogic == null) {
            wallpapersLogic = new WallpapersLogic.Builder()
                .repository(provideWallpapersRepository())
                .adapter(provideWallpapersAdapter())
                .preferences(providePreferencesAdapter())
                .build();
        }

        return wallpapersLogic;
    }

    // repositories (some need context)
    public LanguagesRepository provideLanguagesRepository() {
        if (languagesRepository == null) {
            languagesRepository = new LanguagesRepositoryObject(appContext);
        }

        return languagesRepository;
    }

    public FontsRepository provideFontsRepository() {
        if (fontsRepository == null) {
            fontsRepository = new FontsRepositoryObject(appContext);
        }

        return fontsRepository;
    }

    public WallpapersRepository provideWallpapersRepository() {
        if (wallpapersRepository == null) {
            wallpapersRepository = new WallpapersRepositoryObject(appContext);
        }

        return wallpapersRepository;
    }

    public VersesRepository provideVersesRepository() {
        if (versesRepository == null) {
            versesRepository = new VersesRepositoryObject.Builder()
                .server(provideServerAdapter())
                .database(provideDBAdapter())
                .build();
        }

        return versesRepository;
    }

    // adapters (most need context)
    public PreferencesAdapter providePreferencesAdapter() {
        if (preferencesAdapter == null) {
            preferencesAdapter = new PreferencesAdapterObject(appContext);
        }

        return preferencesAdapter;
    }

    public WallpapersAdapter provideWallpapersAdapter() {
        if (wallpapersAdapter == null) {
            wallpapersAdapter = new WallpapersAdapterObject(appContext);
        }

        return wallpapersAdapter;
    }

    public MusicAdapter provideMusicAdapter() {
        if (musicAdapter == null) {
            musicAdapter = new MusicAdapterObject(appContext);
        }

        return musicAdapter;
    }

    public LocaleAdapter provideLocaleAdapter() {
        if (localeAdapter == null) {
            localeAdapter = new LocaleAdapterObject(appContext);
        }

        return localeAdapter;
    }

    public SpeakingAdapter provideSpeakingAdapter() {
        if (speakingAdapter == null) {
            speakingAdapter = new SpeakingAdapterObject(appContext);
        }

        return speakingAdapter;
    }

    public DBAdapter provideDBAdapter() {
        if (dbAdapter == null) {
            dbAdapter = new DBAdapterObject();
        }

        return dbAdapter;
    }

    public ServerAdapter provideServerAdapter() {
        if (serverAdapter == null) {
            serverAdapter = new ServerAdapterObject();
        }

        return serverAdapter;
    }
}
