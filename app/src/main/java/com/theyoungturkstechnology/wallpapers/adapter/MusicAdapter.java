package com.theyoungturkstechnology.wallpapers.adapter;

import android.support.annotation.VisibleForTesting;

/**
 * Created by dvukotic on 24.01.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public interface MusicAdapter {
    void play();
    void stop();
    Boolean isPlaying();
}
