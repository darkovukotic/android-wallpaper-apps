package com.theyoungturkstechnology.wallpapers.adapter;

import com.theyoungturkstechnology.wallpapers.repository.VersesCallback;
import com.theyoungturkstechnology.wallpapers.model.VersesType;
import com.theyoungturkstechnology.wallpapers.type.CallbackWithResult;

/**
 * Created by dvukotic on 29.01.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public interface DBAdapter {
    void getVerses(VersesType type, VersesCallback caller) throws Exception;
    void saveVerses(CallbackWithResult caller) throws Exception;
}
