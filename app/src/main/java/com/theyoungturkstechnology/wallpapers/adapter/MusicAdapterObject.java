package com.theyoungturkstechnology.wallpapers.adapter;

import android.content.Context;
import android.media.MediaPlayer;

import com.theyoungturkstechnology.wallpapers.R;
import com.theyoungturkstechnology.wallpapers.exception.ForbiddenCodeException;

import java.lang.ref.WeakReference;

/**
 * Created by dvukotic on 24.01.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public class MusicAdapterObject implements MusicAdapter {
    private static final String TAG = MusicAdapterObject.class.getSimpleName() + " * ";
    private WeakReference<Context> wContext;
    private MediaPlayer player = null;
    private final float Volume = 0.3F;

    private MusicAdapterObject() {
        wContext = null;
    }

    public MusicAdapterObject(final WeakReference<Context> _context) {
        if (_context == null) {
            throw new IllegalArgumentException();
        }

        wContext = _context;
    }

    // MusicAdapter
    @Override
    public synchronized void play() {
        Context context = wContext.get();
        if (context == null) {
            throw new ForbiddenCodeException();
        }

        if (player == null) {
            player = MediaPlayer.create(context, R.raw.azan);
            player.setLooping(true);
            player.setVolume(Volume, Volume);
        }

        if (isPlaying()) {
            return;
        }

        player.start();
    }

    @Override
    public synchronized void stop() {
        if (!isPlaying()) {
            return;
        }

        player.pause();
//        player.stop();
//        player.release();
//        player = null;
    }

    @Override
    public synchronized Boolean isPlaying() {
        return player != null && player.isPlaying();
    }
}
