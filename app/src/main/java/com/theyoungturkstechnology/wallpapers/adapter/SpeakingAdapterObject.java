package com.theyoungturkstechnology.wallpapers.adapter;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.SystemClock;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.util.Log;

import com.theyoungturkstechnology.wallpapers.BuildConfig;
import com.theyoungturkstechnology.wallpapers.exception.ForbiddenCodeException;

import java.lang.ref.WeakReference;
import java.util.Locale;

/**
 * Created by dvukotic on 30.01.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public class SpeakingAdapterObject implements SpeakingAdapter, TextToSpeech.OnInitListener {
    private static final String TAG = SpeakingAdapterObject.class.getSimpleName() + " * ";
    private final TextToSpeech engine;
    private volatile Boolean isReady;
    // fixed?
    private final float Pitch = 0.8f;
    private final float Rate = 0.9f;

    private SpeakingAdapterObject() {
        this.engine = null;
    }

    public SpeakingAdapterObject(final WeakReference<Context> _context) {
        if (_context == null) {
            throw new IllegalArgumentException();
        }

        Context context = _context.get();
        if (context == null) {
            throw new ForbiddenCodeException();
        }

        isReady = false;
        engine = new TextToSpeech(context, this);
    }

    private synchronized void setReady() {
        isReady = true;
    }

    private synchronized boolean isReady() {
        return isReady;
    }

    @TargetApi(21)
    private void _start(String text, Locale locale, Runnable onError) {
        final Runnable returnError = () -> {
            if (onError != null) {
                onError.run();
            }
        };

        engine.stop();

        int result = engine.setLanguage(locale);
        if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
            returnError.run();
            return;
        }

        engine.setPitch(Pitch);
        engine.setSpeechRate(Rate);

        engine.setOnUtteranceProgressListener(new UtteranceProgressListener() {
            @Override
            public void onStart(String s) {

            }

            @Override
            public void onDone(String s) {
            }

            @Override
            public void onStop(String utteranceId, boolean interrupted) {

            }

            @Override
            public void onError(String s) {
                returnError.run();
            }
        });

        if (Build.VERSION.SDK_INT < 21) {
            engine.speak(text, TextToSpeech.QUEUE_FLUSH, null);
        } else {
            engine.speak(text, TextToSpeech.QUEUE_FLUSH, null, BuildConfig.APPLICATION_ID);
        }
    }

    private void _isLocaleSupported(Locale locale, Runnable onSupported, Runnable onNotSupported) {
        int supportResult = engine.isLanguageAvailable(locale);

        if (supportResult != TextToSpeech.LANG_MISSING_DATA && supportResult != TextToSpeech.LANG_NOT_SUPPORTED) {
            if (onSupported != null) {
                onSupported.run();
            }
        } else {
            if (onNotSupported != null) {
                onNotSupported.run();

            }
        }
    }

    private void _stop() {
        engine.stop();
    }

    // SpeakingAdapter

    @Override
    public synchronized void isLocaleSupported(Locale locale, Runnable onSupported, Runnable onNotSupported) {
        if (locale == null) {
            throw new IllegalArgumentException();
        }

        if (isReady()) {
            _isLocaleSupported(locale, onSupported, onNotSupported);
            return;
        }

        // poll till ready
        for (int i = 0; i < 30; i++) {
            SystemClock.sleep(200);

            if (isReady()) {
                _isLocaleSupported(locale, onSupported, onNotSupported);
                return;
            }
        }

        // timeout
        if (onNotSupported != null) {
            onNotSupported.run();
        }
    }

    @Override
    public synchronized void start(String text, Locale locale, Runnable onError) {
        if (text == null || text.isEmpty() || locale == null) {
            throw new IllegalArgumentException();
        }

        if (isReady()) {
            _start(text, locale, onError);
            return;
        }

        // poll till ready
        for (int i = 0; i < 30; i++) {
            SystemClock.sleep(200);

            if (isReady()) {
                _start(text, locale, onError);
                return;
            }
        }

        // timeout
        if (onError != null) {
            onError.run();
        }
    }

    @Override
    public synchronized void stop() {
        if (isReady()) {
            _stop();
            return;
        }

        // poll till ready
        for (int i = 0; i < 10; i++) {
            SystemClock.sleep(100);

            if (isReady()) {
                _stop();
                return;
            }
        }
    }

    // TextToSpeech.OnInitListener

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            setReady();
        } else {
            if (engine != null) {
                engine.stop();
                engine.shutdown();
            }

            Log.e(TAG, "Initialization Failed!");
        }
    }

    // SpeakingForTest

    @Override
    public Boolean isSpeaking() {
        return isReady() && engine.isSpeaking();
    }
}
