package com.theyoungturkstechnology.wallpapers.adapter;

import java.util.Locale;

/**
 * Created by dvukotic on 29.01.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public interface LocaleAdapter {
    Locale getLocaleForLanguageCode(String code) throws Exception;
    String getPhoneLanguageCode() throws Exception;
}
