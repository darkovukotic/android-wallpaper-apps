package com.theyoungturkstechnology.wallpapers.adapter;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;

import com.theyoungturkstechnology.wallpapers.exception.ForbiddenCodeException;
import com.theyoungturkstechnology.wallpapers.exception.NoObjectOrEmptyException;

import java.lang.ref.WeakReference;
import java.util.Locale;

/**
 * Created by dvukotic on 29.01.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public class LocaleAdapterObject implements LocaleAdapter {
    private static final String TAG = LocaleAdapterObject.class.getSimpleName() + " * ";
    private final WeakReference<Context> wContext;

    private LocaleAdapterObject() {
        wContext = null;
    }

    public LocaleAdapterObject(final WeakReference<Context> _context) {
        if (_context == null) {
            throw new IllegalArgumentException();
        }

        this.wContext = _context;
    }

    @Override
    @TargetApi(21)
    public Locale getLocaleForLanguageCode(String code) throws Exception {
        if (code == null || code.isEmpty()) {
            throw new IllegalArgumentException();
        }

        Locale locale;

        if (Build.VERSION.SDK_INT < 21) {
            locale = new Locale(code);
        } else {
            locale = Locale.forLanguageTag(code);
        }

        if (locale == null) {
            throw new NoObjectOrEmptyException();
        }

        return locale;
    }

    @Override
    @TargetApi(24)
    public String getPhoneLanguageCode() throws Exception {
        Context context = wContext.get();
        if (context == null) {
            throw new ForbiddenCodeException();
        }

        Locale locale;
        if (Build.VERSION.SDK_INT < 24) {
            locale = context.getResources().getConfiguration().locale;
        } else {
            locale = context.getResources().getConfiguration().getLocales().get(0);
        }

        if (locale == null) {
            throw new NoObjectOrEmptyException();
        }

        return locale.getLanguage();
    }

//    public String getPhoneCountry() {
//        String country = null;
//        TelephonyManager manager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
//        if (manager != null) {
//            country = manager.getSimCountryIso();
//        }
//
//        return country;
//    }

//    public String getOperatorCountry() {
//        String country = null;
//        TelephonyManager manager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
//        if (manager != null) {
//            country = manager.getNetworkCountryIso();
//        }
//
//        return country;
//    }
}
