package com.theyoungturkstechnology.wallpapers.adapter;

import com.theyoungturkstechnology.wallpapers.repository.VersesCallback;
import com.theyoungturkstechnology.wallpapers.model.VersesType;

/**
 * Created by dvukotic on 29.01.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public interface ServerAdapter {
    void getVerses(VersesType type, VersesCallback caller) throws Exception;
}
