package com.theyoungturkstechnology.wallpapers.adapter;

/**
 * Created by dvukotic on 26.12.2017.
 */

@SuppressWarnings("DefaultFileTemplate")
public interface PreferencesAdapter {
    int getLastSelectedWallpaperIndex();
    void saveLastSelectedWallpaperIndex(int index);

    boolean getIsMusicEnabled();
    void saveIsMusicEnabled(boolean enabled);

    String getCurrentLanguageCode();
    void saveCurrentLanguageCode(String code);
}
