package com.theyoungturkstechnology.wallpapers.adapter;

import java.util.Locale;

/**
 * Created by dvukotic on 30.01.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public interface SpeakingAdapter {
    void isLocaleSupported(Locale locale, Runnable onSupported, Runnable onNotSupported);
    void start(String text, Locale locale, Runnable onError);
    void stop();
    Boolean isSpeaking();
}
