package com.theyoungturkstechnology.wallpapers.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.theyoungturkstechnology.wallpapers.exception.ForbiddenCodeException;

import java.lang.ref.WeakReference;

/**
 * Created by dvukotic on 26.12.2017.
 */

@SuppressWarnings("DefaultFileTemplate")
public class PreferencesAdapterObject implements PreferencesAdapter {
    private static final String TAG = PreferencesAdapterObject.class.getSimpleName() + " * ";
    private final WeakReference<Context> wContext;
    private static final String Key_WallpaperIndex = "WallpaperIndex";
    private static final int Default_WallpaperIndex = 0;
    private static final String Key_MusicEnabled = "MusicEnabled";
    private static final boolean Default_MusicEnabled = true;
    private static final String Key_LanguageCode = "LanguageCode";
    private static final String Default_LanguageCode = "";

    private PreferencesAdapterObject() {
        wContext = null;
    }

    public PreferencesAdapterObject(final WeakReference<Context> _context) {
        if (_context == null) {
            throw new IllegalArgumentException();
        }

        this.wContext = _context;
    }

    @Override
    public int getLastSelectedWallpaperIndex() {
        Context context = wContext.get();
        if (context == null) {
            throw new ForbiddenCodeException();
        }

        return PreferenceManager
            .getDefaultSharedPreferences(context)
            .getInt(Key_WallpaperIndex, Default_WallpaperIndex);
    }

    @Override
    public void saveLastSelectedWallpaperIndex(int index) {
        Context context = wContext.get();
        if (context == null) {
            throw new ForbiddenCodeException();
        }

        if (index < 0) {
            throw new IllegalArgumentException();
        }

        SharedPreferences.Editor prefs = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefs
            .putInt(Key_WallpaperIndex, index)
            .apply();
    }

    @Override
    public boolean getIsMusicEnabled() {
        Context context = wContext.get();
        if (context == null) {
            throw new ForbiddenCodeException();
        }

        return PreferenceManager
            .getDefaultSharedPreferences(context)
            .getBoolean(Key_MusicEnabled, Default_MusicEnabled);
    }

    @Override
    public void saveIsMusicEnabled(boolean enabled) {
        Context context = wContext.get();
        if (context == null) {
            throw new ForbiddenCodeException();
        }

        SharedPreferences.Editor prefs = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefs
            .putBoolean(Key_MusicEnabled, enabled)
            .apply();
    }

    @Override
    public String getCurrentLanguageCode() {
        Context context = wContext.get();
        if (context == null) {
            throw new ForbiddenCodeException();
        }

        return PreferenceManager
            .getDefaultSharedPreferences(context)
            .getString(Key_LanguageCode, Default_LanguageCode);
    }

    @Override
    public void saveCurrentLanguageCode(String code) {
        if (code == null || code.isEmpty()) {
            throw new IllegalArgumentException();
        }

        Context context = wContext.get();
        if (context == null) {
            throw new ForbiddenCodeException();
        }

        SharedPreferences.Editor prefs = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefs
            .putString(Key_LanguageCode, code)
            .apply();
    }
}
