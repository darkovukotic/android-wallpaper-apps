package com.theyoungturkstechnology.wallpapers.adapter;

import com.theyoungturkstechnology.wallpapers.model.WallpaperModel;

import java.io.IOException;
import java.util.List;


/**
 * Created by dvukotic on 22.12.2017.
 */

@SuppressWarnings("DefaultFileTemplate")
public interface WallpapersAdapter {
    void setWallpaper(WallpaperModel wallpaper) throws Exception;
}
