package com.theyoungturkstechnology.wallpapers.adapter;

import android.app.WallpaperManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.DisplayMetrics;

import com.theyoungturkstechnology.wallpapers.R;
import com.theyoungturkstechnology.wallpapers.exception.ForbiddenCodeException;
import com.theyoungturkstechnology.wallpapers.model.WallpaperModel;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dvukotic on 22.12.2017.
 */

@SuppressWarnings("DefaultFileTemplate")
public class WallpapersAdapterObject implements WallpapersAdapter {
    protected static final String TAG = WallpapersAdapterObject.class.getSimpleName() + " * ";
    private final WeakReference<Context> wContext;

    private WallpapersAdapterObject() {
        this.wContext = null;
    }

    public WallpapersAdapterObject(final WeakReference<Context>  _context) {
        if (_context == null) {
            throw new IllegalArgumentException();
        }

        this.wContext = _context;
    }

    @Override
    public void setWallpaper(WallpaperModel wallpaper) throws Exception {
        if (wallpaper == null) {
            throw new IllegalArgumentException();
        }

        setWallpaperWithRid(wallpaper.getRid());
    }

    private void setWallpaperWithRid(int rid) throws Exception {
        if (rid < 0) {
            throw new IllegalArgumentException();
        }

        Context context = wContext.get();
        if (context == null) {
            throw new ForbiddenCodeException();
        }

//        Log.d(TAG, "Will set wallpaper with RID = " + rid);

        BitmapFactory.Options options = new BitmapFactory.Options();
        // WARN: lowers quality but helps protecting against "out of memory" error
        // lower quality => used memory goes from 65 -> 85 MB
        // without any cleaning (recycling) it goes to 95 MB
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        Bitmap tempBmp = BitmapFactory.decodeResource(context.getResources(), rid, options);

        // for landscape picture trust dimensions given by the system
        WallpaperManager wallpaperManager = WallpaperManager.getInstance(context);
        int screenWidth = wallpaperManager.getDesiredMinimumWidth();
        int screenHeight = wallpaperManager.getDesiredMinimumHeight();

        // for portrait picture stick to display size
        if (tempBmp.getWidth() < tempBmp.getHeight()) {
            DisplayMetrics metrics = context.getResources().getDisplayMetrics();
            screenWidth = metrics.widthPixels;
            screenHeight = metrics.heightPixels;
        }

        // first scale up/down to the biggest dimension of the desired dimensions
        double imageWidth = tempBmp.getWidth();
        double imageHeight = tempBmp.getHeight();

        double kWidth = screenWidth / imageWidth;
        double kHeight = screenHeight / imageHeight;
        double k = Math.max(kWidth, kHeight);

        // scale
        int scaledBitmapWidth = (int)(k * imageWidth);
        int scaledBitmapHeight = (int)(k * imageHeight);

        // WARN: rounding is dangerous
        // no bitmap dimension should be smaller than a corresponding screen dimension
        if (scaledBitmapWidth < screenWidth)
            scaledBitmapWidth = screenWidth;
        if (scaledBitmapHeight < screenHeight)
            scaledBitmapHeight = screenHeight;

        Bitmap scaledBitmap = Bitmap.createScaledBitmap (
            tempBmp,
            scaledBitmapWidth,
            scaledBitmapHeight,
            true
        );

        // they might change
        scaledBitmapWidth = scaledBitmap.getWidth();
        scaledBitmapHeight = scaledBitmap.getHeight();

        if (scaledBitmap != tempBmp && !tempBmp.isRecycled()) {
            tempBmp.recycle();
        }

        // now crop center
        Bitmap bitmap = Bitmap.createBitmap(
            scaledBitmap,
            (scaledBitmapWidth - screenWidth)/2,
            (scaledBitmapHeight - screenHeight)/2,
            screenWidth,
            screenHeight
        );

        if (bitmap != scaledBitmap && !scaledBitmap.isRecycled()) {
            scaledBitmap.recycle();
        }

        wallpaperManager.setBitmap(bitmap);

        if (!bitmap.isRecycled()) {
            bitmap.recycle();
        }
    }
}
