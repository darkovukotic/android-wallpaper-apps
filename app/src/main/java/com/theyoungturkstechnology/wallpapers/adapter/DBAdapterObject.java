package com.theyoungturkstechnology.wallpapers.adapter;

import com.theyoungturkstechnology.wallpapers.MOCK.DBAdapterMock;
import com.theyoungturkstechnology.wallpapers.exception.NotImplementedException;
import com.theyoungturkstechnology.wallpapers.model.VersesType;
import com.theyoungturkstechnology.wallpapers.repository.VersesCallback;
import com.theyoungturkstechnology.wallpapers.type.CallbackWithResult;

/**
 * Created by dvukotic on 09.03.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
//public class DBAdapterObject implements DBAdapter {
//    @Override
//    public void getVerses(VersesType type, VersesCallback caller) throws Exception {
//        throw new NotImplementedException("DBAdapterObject::getVerses() not implemented");
//    }
//
//    @Override
//    public void saveVerses(CallbackWithResult caller) throws Exception {
//        throw new NotImplementedException("DBAdapterObject::saveVerses() not implemented");
//    }
//}

public class DBAdapterObject extends DBAdapterMock implements DBAdapter {

}
