package com.theyoungturkstechnology.wallpapers.model;

/**
 * Created by dvukotic on 21.12.2017.
 */

@SuppressWarnings("DefaultFileTemplate")
public class WallpaperModel {
    private static final String TAG = WallpaperModel.class.getSimpleName() + " * ";
    private int rid;

    private WallpaperModel() {}

    public WallpaperModel(int _rid) {
        if (_rid < 0) {
            throw new IllegalArgumentException();
        }

        this.rid = _rid;
    }

    public int getRid() {
        return rid;
    }

    public String toString() {
        return "" + rid;
    }
}
