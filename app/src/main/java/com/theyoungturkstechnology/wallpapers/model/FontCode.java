package com.theyoungturkstechnology.wallpapers.model;

/**
 * Created by dvukotic on 06.03.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public enum FontCode {
    LatinFont,
    HindiFont,
    CyrillicFont,
    ArabicFont
}
