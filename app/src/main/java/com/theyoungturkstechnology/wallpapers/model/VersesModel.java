package com.theyoungturkstechnology.wallpapers.model;

/**
 * Created by dvukotic on 12.02.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public class VersesModel {
    private static final String TAG = VersesModel.class.getSimpleName() + " * ";
    private String versesStr;
    public static final VersesModel Empty = new VersesModel();

    private VersesModel() {
        this.versesStr = "";
    }

    public VersesModel(String _versesStr) {
        if (_versesStr == null || _versesStr.isEmpty()) {
            throw new IllegalArgumentException();
        }

        this.versesStr = _versesStr;
    }

    public String getVersesStr() {
        return versesStr;
    }

    public String toString() {
        return versesStr;
    }
}
