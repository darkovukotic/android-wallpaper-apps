package com.theyoungturkstechnology.wallpapers.model;

/**
 * Created by dvukotic on 29.01.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public class LanguageModel {
    private static final String TAG = LanguageModel.class.getSimpleName() + " * ";
    private String code = null, name = null;
    private FontCode preferredFontCode;

    private LanguageModel() {}

    public LanguageModel(String _code, String _name, FontCode _preferredFontCode) {
        if (_code == null || _code.isEmpty() || _name == null || _name.isEmpty()) {
            throw new IllegalArgumentException();
        }

        this.code = _code;
        this.name = _name;
        this.preferredFontCode = _preferredFontCode;
    }

    public String getCode() {
        return code;
    }
    public String getName() {
        return name;
    }
    public FontCode getPreferredFontCode() {
        return preferredFontCode;
    }

    public String toString() {
        return code + "\t" + name + "\t" + preferredFontCode;
    }
}
