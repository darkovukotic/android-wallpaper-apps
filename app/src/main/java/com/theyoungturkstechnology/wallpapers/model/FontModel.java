package com.theyoungturkstechnology.wallpapers.model;

import android.graphics.Typeface;

/**
 * Created by dvukotic on 06.02.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public class FontModel {
    private static final String TAG = FontModel.class.getSimpleName() + " * ";

    private Typeface typeface = null;
    private FontCode code;
    private int preferredSize = -1;

    private FontModel() {}

    public FontModel(FontCode _code, Typeface _typeface, int _preferredSize) {
        if (_typeface == null || _preferredSize < 0) {
            throw new IllegalArgumentException();
        }

        this.code = _code;
        this.typeface = _typeface;
        this.preferredSize = _preferredSize;
    }

    public FontCode getCode() {
        return code;
    }

    public Typeface getTypeface() {
        return typeface;
    }

    public int getPreferredSize() {
        return preferredSize;
    }

    public String toString() {
        return "" + code + "\t" + typeface + "\t" + preferredSize;
    }
}
