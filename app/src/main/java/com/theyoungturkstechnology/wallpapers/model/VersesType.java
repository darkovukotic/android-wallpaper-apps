package com.theyoungturkstechnology.wallpapers.model;

/**
 * Created by dvukotic on 09.03.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
public enum VersesType {
    Current,
    Random,
    Next
}
