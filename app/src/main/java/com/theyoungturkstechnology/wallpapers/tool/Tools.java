package com.theyoungturkstechnology.wallpapers.tool;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import java.util.Random;

/**
 * Created by dvukotic on 25.12.2017.
 */

@SuppressWarnings("DefaultFileTemplate")
public class Tools {
    private static final String TAG = Tools.class.getSimpleName() + " * ";

    public static int getRandomPositiveInt() {
        int rnd = (new Random()).nextInt();
        rnd = rnd < 0 ? -1 * rnd : rnd;
        return rnd;
    }

    public static void executeDelayedOnUiThread (long delayMs, Runnable runnable) {
        if (runnable == null || delayMs < 0) {
            throw new IllegalArgumentException();
        }

        new Handler(Looper.getMainLooper()).postDelayed(runnable, delayMs);
    }

    public static void executeOnUiThread (Runnable runnable) {
        if (runnable == null) {
            throw new IllegalArgumentException();
        }

        new Handler(Looper.getMainLooper()).post(runnable);
    }

    public static void executeOnNewThread (Runnable runnable) {
        if (runnable == null) {
            throw new IllegalArgumentException();
        }

        new Thread(runnable).run();
    }

    public static void executeDelayedOnNewThread (long delayMs, Runnable runnable) {
        if (runnable == null) {
            throw new IllegalArgumentException();
        }

        executeDelayedOnUiThread(
            delayMs,
            ()-> new Thread(runnable).run()
        );
    }
}
