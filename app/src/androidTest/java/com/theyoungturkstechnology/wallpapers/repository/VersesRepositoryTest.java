package com.theyoungturkstechnology.wallpapers.repository;

import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.theyoungturkstechnology.wallpapers.MOCK.DBAdapterMock;
import com.theyoungturkstechnology.wallpapers.MOCK.ServerAdapterMock;
import com.theyoungturkstechnology.wallpapers.adapter.DBAdapter;
import com.theyoungturkstechnology.wallpapers.adapter.ServerAdapter;
import com.theyoungturkstechnology.wallpapers.model.VersesModel;
import com.theyoungturkstechnology.wallpapers.model.VersesType;
import com.theyoungturkstechnology.wallpapers.type.CallbackResult;
import com.theyoungturkstechnology.wallpapers.type.CallbackWithResult;
import com.theyoungturkstechnology.wallpapers.view.MainActivity;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.stubbing.Answer;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;


/**
 * Created by dvukotic on 12.03.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
@LargeTest
@RunWith(AndroidJUnit4.class)
public class VersesRepositoryTest {
    @Rule public final ExpectedException exception = ExpectedException.none();
    @Rule public ActivityTestRule<MainActivity> testActivity = new ActivityTestRule<>(MainActivity.class);
    @Rule public MockitoRule mockito = MockitoJUnit.rule();
    private VersesRepository repository;
    @Mock private DBAdapter db;
    @Mock private ServerAdapter server;

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testRandomVersesFromServer() throws Throwable {
        repository = new VersesRepositoryObject.Builder()
            .server(new ServerAdapterMock())
            .database(new DBAdapterMock())
            .build();

        repository.getVerses(
            VersesType.Random,
            (CallbackResult result, VersesSource source, VersesType type, VersesModel verses) -> {
                assertEquals(CallbackResult.Success, result);
                assertEquals(VersesType.Random, type);
                assertEquals(VersesSource.Server, source);
                assertNotNull(verses);
                String versesStr = verses.getVersesStr();
                assertNotNull(versesStr);
                assertFalse(versesStr.isEmpty());

                assertTrue(versesStr.contains("Random verses from server"));
            }
        );
    }

    @Test
    public void testNextVersesFromServer() throws Throwable {
        repository = new VersesRepositoryObject.Builder()
            .server(new ServerAdapterMock())
            .database(new DBAdapterMock())
            .build();

        repository.getVerses(
            VersesType.Next,
            (CallbackResult result, VersesSource source, VersesType type, VersesModel verses) -> {
                assertEquals(CallbackResult.Success, result);
                assertEquals(VersesType.Next, type);
                assertEquals(VersesSource.Server, source);
                assertNotNull(verses);
                String versesStr = verses.getVersesStr();
                assertNotNull(versesStr);
                assertFalse(versesStr.isEmpty());

                assertTrue(versesStr.contains("Next verses from server"));
            }
        );
    }

    @Test
    public void testCurrentVerses() throws Throwable {
        repository = new VersesRepositoryObject.Builder()
            .server(new ServerAdapterMock())
            .database(new DBAdapterMock())
            .build();

        // first time it should return error
        repository.getVerses(
            VersesType.Current,
            (CallbackResult result, VersesSource source, VersesType type, VersesModel verses) -> {
                assertEquals(VersesType.Current, type);
                assertEquals(CallbackResult.Error, result);

                // now call for next verses
                try {
                    repository.getVerses(
                        VersesType.Next,
                        (CallbackResult result1, VersesSource source1, VersesType type1, VersesModel verses1) -> {
                            assertEquals(CallbackResult.Success, result1);
                            assertEquals(VersesType.Next, type1);

                            // and finally we should get current verses
                            try {
                                repository.getVerses(
                                    VersesType.Current,
                                    (CallbackResult result2, VersesSource source2, VersesType type2, VersesModel verses2) -> {
                                        assertEquals(VersesType.Current, type2);
                                        assertEquals(CallbackResult.Success, result2);
                                        assertNotNull(verses2);
                                        String versesStr = verses2.getVersesStr();
                                        assertNotNull(versesStr);
                                        assertFalse(versesStr.isEmpty());

                                        assertTrue(versesStr.contains("Next verses from server"));
                                    }
                                );
                            } catch (Exception e) {
                                e.printStackTrace();
                                fail();
                            }
                        }
                    );
                } catch (Exception e) {
                    e.printStackTrace();
                    fail();
                }
            }
        );
    }

    @Test
    public void ifServerFailsItShouldFetchFromDB() throws Throwable {
        // WARN: it doesn't work!?
//        doAnswer(
//            (InvocationOnMock invocation) -> {
//                VersesType type = (VersesType) invocation.getArgument(0);
//                VersesCallback callback = (VersesCallback) invocation.getArgument(1);
//                callback.versesCallback(CallbackResult.Error, VersesSource.Server, type, VersesModel.Empty);
//                return null;
//            }
//        )
//        .when(server).getVerses(any(VersesType.class), any(VersesCallback.class));
//
//        repository = new VersesRepositoryObject.Builder()
//            .server(server)
//            .database(new DBAdapterMock())
//            .build();

        repository = new VersesRepositoryObject.Builder()
            .database(new DBAdapterMock())
            .server( (VersesType type, VersesCallback caller) -> caller.versesCallback(CallbackResult.Error, VersesSource.Server, type, VersesModel.Empty) )
            .build();

        repository.getVerses(
            VersesType.Next,
            (CallbackResult result, VersesSource source, VersesType type, VersesModel verses) -> {
                assertEquals(CallbackResult.Success, result);
                assertEquals(VersesType.Next, type);
                assertEquals(VersesSource.DB, source);
                assertNotNull(verses);
                String versesStr = verses.getVersesStr();
                assertNotNull(versesStr);
                assertFalse(versesStr.isEmpty());

                assertTrue(versesStr.contains("Next verses from DB"));
            }
        );
    }

    @Test
    public void ifBothServerAndDBFailItShouldReturnError() throws Throwable {
        repository = new VersesRepositoryObject.Builder()
            .server( (VersesType type, VersesCallback caller) -> caller.versesCallback(CallbackResult.Error, VersesSource.Server, type, VersesModel.Empty) )
            .database(
                new DBAdapter() {
                    @Override
                    public void getVerses(VersesType type, VersesCallback caller) throws Exception {
                        caller.versesCallback(CallbackResult.Error, VersesSource.DB, type, VersesModel.Empty);
                    }

                    @Override
                    public void saveVerses(CallbackWithResult caller) throws Exception {}
                }
            )
            .build();

        repository.getVerses(
            VersesType.Random,
            (CallbackResult result, VersesSource source, VersesType type, VersesModel verses) -> {
                assertEquals(CallbackResult.Error, result);
                assertEquals(VersesType.Random, type);
                assertEquals(VersesSource.DB, source);
                assertNotNull(verses);
            }
        );
    }
}

