package com.theyoungturkstechnology.wallpapers.logic;

import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.theyoungturkstechnology.wallpapers.model.FontCode;
import com.theyoungturkstechnology.wallpapers.model.LanguageModel;
import com.theyoungturkstechnology.wallpapers.logic.LanguagesLogic;
import com.theyoungturkstechnology.wallpapers.provider.Provider;
import com.theyoungturkstechnology.wallpapers.view.MainActivity;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import java.util.List;
import java.util.Locale;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by dvukotic on 29.01.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
@LargeTest
@RunWith(AndroidJUnit4.class)
public class LanguagesLogicTest {
    @Rule public final ExpectedException exception = ExpectedException.none();
    @Rule public ActivityTestRule<MainActivity> testActivity = new ActivityTestRule<>(MainActivity.class);
    private LanguagesLogic languages = null;

    @Before
    public void setUp() throws Exception {
        Provider provider = Provider.instance();
        provider.setAppContext(testActivity.getActivity().getApplicationContext());
        languages = provider.provideLanguagesLogic();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void shouldGetCurrentLanguage() throws Exception {
        LanguageModel lang = new LanguageModel("en", "English", FontCode.LatinFont);
        languages.saveCurrentLanguage(lang);

        LanguageModel language = languages.getCurrentLanguage();
        assertEquals("en", language.getCode());
    }

    @Test
    public void shouldGetLanguages() throws Exception {
        List<LanguageModel> languageList = languages.getLanguages();
        assertNotNull(languageList);
        assertFalse(languageList.isEmpty());
    }

    @Test
    public void shouldGetLocale() throws Exception {
        LanguageModel language = languages.getCurrentLanguage();
        Locale locale1 = languages.getLocaleForCurrentLanguage();
        Locale locale2 = languages.getLocaleForLanguage(language);
        assertEquals(locale1, locale2);
    }

    @Test
    public void shouldHaveLanguages() throws Exception {
        List<LanguageModel> languageList = languages.getLanguages();

        assertTrue(languageList != null && !languageList.isEmpty());
        assertEquals(6, languageList.size());
    }

    @Test
    public void shouldGetLocaleForLanguage() throws Exception {
        LanguageModel language = languages.getLanguages().get(0);

        Locale locale = languages.getLocaleForLanguage(language);
        assertNotNull(locale);
        assertTrue(locale.getDisplayName().contains("English"));
    }

    @Test
    public void shouldSaveCurrentLanguage() throws Exception {
        LanguageModel origLanguage = languages.getCurrentLanguage();
        assertNotNull(origLanguage);
        assertEquals("en", origLanguage.getCode());

        LanguageModel newLanguage = new LanguageModel("tr", "Turkish", FontCode.LatinFont);
        languages.saveCurrentLanguage(newLanguage);
        LanguageModel language = languages.getCurrentLanguage();
        assertNotNull(language);
        assertEquals("tr", language.getCode());

        languages.saveCurrentLanguage(origLanguage);
    }
}
