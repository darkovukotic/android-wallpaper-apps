package com.theyoungturkstechnology.wallpapers.logic;

import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.theyoungturkstechnology.wallpapers.model.FontCode;
import com.theyoungturkstechnology.wallpapers.model.FontModel;
import com.theyoungturkstechnology.wallpapers.logic.FontsLogic;
import com.theyoungturkstechnology.wallpapers.provider.Provider;
import com.theyoungturkstechnology.wallpapers.view.MainActivity;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by dvukotic on 06.02.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
@LargeTest
@RunWith(AndroidJUnit4.class)
public class FontsLogicTest {
    @Rule public final ExpectedException exception = ExpectedException.none();
    @Rule public ActivityTestRule<MainActivity> testActivity = new ActivityTestRule<>(MainActivity.class);
    private FontsLogic fonts = null;

    @Before
    public void setUp() throws Exception {
        Provider provider = Provider.instance();
        provider.setAppContext(testActivity.getActivity().getApplicationContext());
        fonts = provider.provideFontsLogic();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getFontByCodeWorks() throws Exception {
        FontModel font = fonts.getFontWithCode(FontCode.ArabicFont);
        assertNotNull(font);
        assertEquals(FontCode.ArabicFont, font.getCode());
        assertEquals(25, font.getPreferredSize());

        font = fonts.getFontWithCode(FontCode.CyrillicFont);
        assertNotNull(font);
        assertEquals(FontCode.CyrillicFont, font.getCode());
        assertEquals(24, font.getPreferredSize());

        font = fonts.getFontWithCode(FontCode.HindiFont);
        assertNotNull(font);
        assertEquals(FontCode.HindiFont, font.getCode());
        assertEquals(27, font.getPreferredSize());

        font = fonts.getFontWithCode(FontCode.LatinFont);
        assertNotNull(font);
        assertEquals(FontCode.LatinFont, font.getCode());
        assertEquals(20, font.getPreferredSize());
    }
}
