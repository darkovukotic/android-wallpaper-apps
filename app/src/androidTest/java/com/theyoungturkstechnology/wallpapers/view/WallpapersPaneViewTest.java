package com.theyoungturkstechnology.wallpapers.view;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import com.theyoungturkstechnology.wallpapers.R;
import com.theyoungturkstechnology.wallpapers.adapter.PreferencesAdapter;
import com.theyoungturkstechnology.wallpapers.adapter.WallpapersAdapter;
import com.theyoungturkstechnology.wallpapers.model.WallpaperModel;
import com.theyoungturkstechnology.wallpapers.logic.WallpapersLogic;
import com.theyoungturkstechnology.wallpapers.presenter.WallpapersPaneContract.Presenter;

import com.theyoungturkstechnology.wallpapers.presenter.WallpapersPanePresenter;
import com.theyoungturkstechnology.wallpapers.type.CallbackResult;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import static org.mockito.Mockito.*;

/**
 * Created by dvukotic on 05.01.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
@RunWith(AndroidJUnit4.class)
@LargeTest
public class WallpapersPaneViewTest {
    @Rule public final ExpectedException exception = ExpectedException.none();
    @Rule public MockitoRule mockito = MockitoJUnit.rule();
    @Rule public ActivityTestRule<MainActivity> testActivity = new ActivityTestRule<>(MainActivity.class);

    private MainActivity activity;
    @Mock private Presenter presenter;
    @Mock private PreferencesAdapter preferences ;
    @Mock private WallpapersAdapter wallpapersAdapter;
    private ArrayList<WallpaperModel> listOf3Wallpapers;

    @Before
    public void setUp() throws Exception {
        activity = testActivity.getActivity();

        listOf3Wallpapers = new ArrayList<>();
        listOf3Wallpapers.add(new WallpaperModel(R.mipmap.fox01));
        listOf3Wallpapers.add(new WallpaperModel(R.mipmap.fox02));
        listOf3Wallpapers.add(new WallpaperModel(R.mipmap.fox03));
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void onViewResumeWillCallViewDidLoadInFlipper() throws Throwable {
        WallpapersPaneView view = new WallpapersPaneView(new WeakReference<>(activity), R.id.wallpapersPaneRootView);
        WallpapersLogic wallpapers = new WallpapersLogic.Builder()
            .preferences(preferences)
            .adapter(wallpapersAdapter)
            .repository((caller) -> caller.wallpapersCallback(CallbackResult.Success, listOf3Wallpapers))
            .build();
        presenter = spy(
            new WallpapersPanePresenter.Builder()
                .view(view)
                .preferences(preferences)
                .wallpapers(wallpapers)
                .build()
        );
        view.setPresenter(presenter);

        testActivity.runOnUiThread(()->
            {
                view.load();
                verify(presenter).viewDidShow();
            }
        );
    }

    @Test
    public void onViewResumeWillCallShowInFlipperWallpaperWithIndexInActivity() throws Throwable {
        WallpapersPaneView view = spy(new WallpapersPaneView(new WeakReference<>(activity), R.id.wallpapersPaneRootView));

        WallpapersLogic wallpapers = new WallpapersLogic.Builder()
            .preferences(preferences)
            .adapter(wallpapersAdapter)
            .repository((caller) -> caller.wallpapersCallback(CallbackResult.Success, listOf3Wallpapers))
            .build();
        presenter = new WallpapersPanePresenter.Builder()
            .view(view)
            .preferences(preferences)
            .wallpapers(wallpapers)
            .build();
        view.setPresenter(presenter);

        testActivity.runOnUiThread(()->
            {
                view.load();
                verify(view).showInFlipperWallpaperWithIndex(anyInt());
            }
        );
    }
}