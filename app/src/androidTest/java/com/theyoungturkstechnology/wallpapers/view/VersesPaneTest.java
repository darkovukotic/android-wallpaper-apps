package com.theyoungturkstechnology.wallpapers.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.SystemClock;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.util.TypedValue;
import android.widget.TextView;

import com.theyoungturkstechnology.wallpapers.R;
import com.theyoungturkstechnology.wallpapers.model.FontCode;
import com.theyoungturkstechnology.wallpapers.model.FontModel;
import com.theyoungturkstechnology.wallpapers.logic.FontsLogic;
import com.theyoungturkstechnology.wallpapers.model.LanguageModel;
import com.theyoungturkstechnology.wallpapers.logic.LanguagesLogic;
import com.theyoungturkstechnology.wallpapers.provider.Provider;
import com.theyoungturkstechnology.wallpapers.adapter.SpeakingAdapter;

import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import java.lang.ref.WeakReference;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isEnabled;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.theyoungturkstechnology.wallpapers.view.PaneView.ErrorTryLaterMessage;
import static org.junit.Assert.assertEquals;
import static org.mockito.AdditionalMatchers.not;

/**
 * Created by dvukotic on 16.01.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
@RunWith(AndroidJUnit4.class)
@LargeTest
public class VersesPaneTest {
    @Rule public final ExpectedException exception = ExpectedException.none();
    @Rule public ActivityTestRule<MainActivity> testActivity = new ActivityTestRule<>(MainActivity.class);
    private SpeakingAdapter speaking = null;
    private LanguagesLogic languages = null;
    private FontsLogic fonts = null;

    private float convertSpToPixels(float sp, Context context) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, context.getResources().getDisplayMetrics());
    }

    @Before
    public void setUp() throws Exception {
        languages = Provider.instance().provideLanguagesLogic();
        speaking = Provider.instance().provideSpeakingAdapter();
        fonts = Provider.instance().provideFontsLogic();

        languages.saveCurrentLanguage(languages.getLanguages().get(0));
    }

    @After
    public void tearDown() throws Exception {
        languages.saveCurrentLanguage(languages.getLanguages().get(0));
        speaking.stop();
    }

    @Test
    public void onEveryLoadBackgroundShouldChange() throws Exception {
        Intent intent = testActivity.getActivity().getIntent();

        Thread.sleep(2*1000);
        testActivity.finishActivity();

        testActivity.launchActivity(intent);
        Thread.sleep(2*1000);
        testActivity.finishActivity();

        testActivity.launchActivity(intent);
        Thread.sleep(2*1000);
        testActivity.finishActivity();

        testActivity.launchActivity(intent);
        Thread.sleep(2*1000);
    }

    @Test
    public void showMessage() throws Throwable {
        VersesPaneView view = new VersesPaneView(new WeakReference<>(testActivity.getActivity()), R.id.versesPaneRootView);
        testActivity.runOnUiThread(
            () -> view.showMessage(ErrorTryLaterMessage)
        );
    }

    @Test
    public void clickOnReadButtonStartsSpeaking() throws Throwable {
        testActivity.runOnUiThread(
            ()-> testActivity.getActivity().loadPane(PaneType.Verses)
        );

        SystemClock.sleep(1500);
        assertEquals(false, speaking.isSpeaking());

        SystemClock.sleep(500);
        onView(ViewMatchers.withId(R.id.readVersesBtn)).perform(click());
        SystemClock.sleep(500);
        assertEquals(true, speaking.isSpeaking());

        SystemClock.sleep(5000);

        onView(ViewMatchers.withId(R.id.readVersesBtn)).perform(click());
        SystemClock.sleep(500);
        assertEquals(false, speaking.isSpeaking());
    }

    @Test
    public void secondClickOnReadButtonStopsSpeaking() throws Throwable {
        testActivity.runOnUiThread(()->
            testActivity.getActivity().loadPane(PaneType.Verses)
        );

        SystemClock.sleep(1500);
        assertEquals(false, speaking.isSpeaking());

        onView(withId(R.id.readVersesBtn)).perform(click());
        SystemClock.sleep(500);
        assertEquals(true, speaking.isSpeaking());

        SystemClock.sleep(2000);

        onView(withId(R.id.readVersesBtn)).perform(click());
        SystemClock.sleep(500);
        assertEquals(false, speaking.isSpeaking());

        SystemClock.sleep(2000);
    }

    @Test
    public void showErrorMessageForSpeakingUnsupportedLanguageAndDisableReadButton() throws Throwable {
        testActivity.runOnUiThread(()->
            testActivity.getActivity().loadPane(PaneType.Verses)
        );

        languages.saveCurrentLanguage(new LanguageModel("ar", "Arabic", FontCode.ArabicFont));

        // force
        testActivity.runOnUiThread(
            () -> (testActivity.getActivity().findViewById(R.id.readVersesBtn)).setEnabled(true)
        );

        SystemClock.sleep(1500);

        onView(withId(R.id.readVersesBtn)).perform(click());
        onView(withId(R.id.readVersesBtn)).check(matches(Matchers.not(isEnabled())));

        SystemClock.sleep(3000);
    }

    @Test
    public void onLoadReadButtonShouldBeEnabledForSpeakingSupportedLanguage() throws Throwable {
        Intent intent = testActivity.getActivity().getIntent();
        testActivity.finishActivity();
        testActivity.launchActivity(intent);

        testActivity.runOnUiThread(()->
            testActivity.getActivity().loadPane(PaneType.Verses)
        );

        onView(withId(R.id.readVersesBtn)).check(matches(isEnabled()));
    }

    @Test
    public void onLoadReadButtonShouldBeDisabledForSpeakingUnsupportedLanguage() throws Throwable {
        languages.saveCurrentLanguage(new LanguageModel("ar", "Arabic", FontCode.ArabicFont));

        Intent intent = testActivity.getActivity().getIntent();
        testActivity.finishActivity();
        testActivity.launchActivity(intent);

        testActivity.runOnUiThread(()->
            testActivity.getActivity().loadPane(PaneType.Verses)
        );

        onView(withId(R.id.readVersesBtn)).check(matches(Matchers.not(isEnabled())));

        SystemClock.sleep(3000);
        languages.saveCurrentLanguage(new LanguageModel("en", "English", FontCode.LatinFont));
    }

    @Test
    public void onLoadRandomVersesShouldBeShowed() throws Throwable {
        testActivity.runOnUiThread(()->
            testActivity.getActivity().loadPane(PaneType.Verses)
        );

        SystemClock.sleep(1500);
        onView(withId(R.id.verses)).check(matches(withText("Random verses from server 1 Random verses from server 2 Random verses from server 3 Random verses from server 4")));
    }

    @Test
    public void differentLanguagesDifferentLocaleAndFonts() throws Throwable{
        Intent intent = testActivity.getActivity().getIntent();

        testActivity.runOnUiThread(()->
            testActivity.getActivity().loadPane(PaneType.Verses)
        );

        for (LanguageModel language : languages.getLanguages()) {
            FontModel font = fonts.getFontWithCode(language.getPreferredFontCode());
            languages.saveCurrentLanguage(language);

            testActivity.finishActivity();
            testActivity.launchActivity(intent);

            testActivity.runOnUiThread(()->
                testActivity.getActivity().loadPane(PaneType.Verses)
            );

            Activity activity = testActivity.getActivity();
            TextView tv = activity.findViewById(R.id.verses);

            SystemClock.sleep(3000);

            float tvTextSize = tv.getTextSize();
            Typeface tvTypeface = tv.getTypeface();
            float fontSize = convertSpToPixels(font.getPreferredSize(), activity);
            assertEquals(fontSize, tvTextSize, 0);
            assertEquals(font.getTypeface(), tvTypeface);
        }
    }

}
