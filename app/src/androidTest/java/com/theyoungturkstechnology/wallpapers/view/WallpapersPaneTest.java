package com.theyoungturkstechnology.wallpapers.view;

import android.content.Intent;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.widget.AdapterViewFlipper;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import com.theyoungturkstechnology.wallpapers.R;

import java.lang.ref.WeakReference;

import static android.support.test.espresso.Espresso.*;
import static android.support.test.espresso.action.ViewActions.*;
import static android.support.test.espresso.matcher.ViewMatchers.*;
import static com.theyoungturkstechnology.wallpapers.view.PaneView.ErrorTryLaterMessage;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

/**
 * Created by dvukotic on 16.01.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
@RunWith(AndroidJUnit4.class)
@LargeTest
public class WallpapersPaneTest {

    @Rule public final ExpectedException exception = ExpectedException.none();
    @Rule public ActivityTestRule<MainActivity> testActivity = new ActivityTestRule<>(MainActivity.class);

    @Before
    public void setUp() throws Throwable {
        testActivity.runOnUiThread(()-> testActivity.getActivity().loadPane(PaneType.Wallpapers));
    }

    @Test
    public void clickOnSetWallpaperButtonShouldCallSelectedWallpaperWithIndexInPresenter() {
        onView(withId(R.id.setWallpaperBtn)).perform(click());
//        TODO: how to test if wallpaper has been changed? At least it catches exception
    }

    @Test
    public void clickOnPrevNextButtonsChangeCurrentShownWallpaperIndex() {
        AdapterViewFlipper imagesAdapter = testActivity.getActivity().findViewById(R.id.flipperView);

        int oldIndex = imagesAdapter.getDisplayedChild();
        onView(withId(R.id.nextWallpaperBtn)).perform(click());
        int newIndex = imagesAdapter.getDisplayedChild();
        assertEquals(oldIndex, (newIndex + 1)%imagesAdapter.getChildCount());

        onView(withId(R.id.previousWallpaperBtn)).perform(click());
        newIndex = imagesAdapter.getDisplayedChild();
        assertEquals(oldIndex, newIndex);
    }

    @Test
    public void onEveryLoadBackgroundShouldChange() throws Exception {
        Intent intent = testActivity.getActivity().getIntent();

        Thread.sleep(2*1000);
        testActivity.finishActivity();

        testActivity.launchActivity(intent);
        Thread.sleep(2*1000);
        testActivity.finishActivity();

        testActivity.launchActivity(intent);
        Thread.sleep(2*1000);
        testActivity.finishActivity();

        testActivity.launchActivity(intent);
        Thread.sleep(2*1000);
    }

    @Test
    public void showMessage() throws Throwable {
        WallpapersPaneView view = new WallpapersPaneView(new WeakReference<>(testActivity.getActivity()), R.id.wallpapersPaneRootView);
        testActivity.runOnUiThread(
            () -> view.showMessage(ErrorTryLaterMessage)
        );
    }
}
