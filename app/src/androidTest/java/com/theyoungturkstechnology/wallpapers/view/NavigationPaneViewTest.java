package com.theyoungturkstechnology.wallpapers.view;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.theyoungturkstechnology.wallpapers.R;
import com.theyoungturkstechnology.wallpapers.model.WallpaperModel;
import com.theyoungturkstechnology.wallpapers.logic.WallpapersLogic;
import com.theyoungturkstechnology.wallpapers.presenter.NavigationPaneContract;
import com.theyoungturkstechnology.wallpapers.presenter.NavigationPanePresenter;
import com.theyoungturkstechnology.wallpapers.adapter.MusicAdapter;
import com.theyoungturkstechnology.wallpapers.adapter.PreferencesAdapter;
import com.theyoungturkstechnology.wallpapers.adapter.SpeakingAdapter;
import com.theyoungturkstechnology.wallpapers.adapter.WallpapersAdapter;
import com.theyoungturkstechnology.wallpapers.repository.WallpapersRepository;
import com.theyoungturkstechnology.wallpapers.type.CallbackResult;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import static org.mockito.Mockito.*;

/**
 * Created by dvukotic on 15.02.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
@android.support.test.filters.LargeTest
@RunWith(AndroidJUnit4.class)
public class NavigationPaneViewTest {
    @Rule public final ExpectedException exception = ExpectedException.none();
    @Rule public MockitoRule mockito = MockitoJUnit.rule();
    @Rule public ActivityTestRule<MainActivity> testActivity = new ActivityTestRule<>(MainActivity.class);

    private MainActivity activity;
    @Mock private NavigationPaneContract.Presenter presenter;
    @Mock private WallpapersAdapter wallpapersAdapter;
    @Mock private WallpapersRepository wallpapersRepository;
    @Mock private PreferencesAdapter preferences;
    @Mock private MusicAdapter music;
    @Mock private SpeakingAdapter speaking;
    @Mock private NavigationPaneContract.View mockedView;
    private ArrayList<WallpaperModel> listOf3Wallpapers;

    @Before
    public void setUp() throws Throwable {
        activity = testActivity.getActivity();

        listOf3Wallpapers = new ArrayList<>();
        listOf3Wallpapers.add(new WallpaperModel(R.mipmap.fox01));
        listOf3Wallpapers.add(new WallpaperModel(R.mipmap.fox02));
        listOf3Wallpapers.add(new WallpaperModel(R.mipmap.fox03));
    }

    @After
    public void tearDown() throws Throwable {
    }

    private void executeOnUiThread (Runnable runnable) throws Throwable {
        testActivity.runOnUiThread(runnable);
    }

    @Test
    public void onAppStartShouldSetBackgroundPicture() throws Throwable {
        NavigationPaneView view = spy(new NavigationPaneView(new WeakReference<>(activity), R.id.navigationPaneRootView));

        WallpapersLogic wallpapers = new WallpapersLogic.Builder()
            .preferences(preferences)
            .adapter(wallpapersAdapter)
            .repository((caller) -> caller.wallpapersCallback(CallbackResult.Success, listOf3Wallpapers))
            .build();

        presenter = new NavigationPanePresenter.Builder()
            .view(view)
            .preferences(preferences)
            .wallpapers(wallpapers)
            .build();

        view.setPresenter(presenter);

        executeOnUiThread(() -> {
            view.onAppActive();
            verify(view).setBackgroundImage(anyInt());
        });
    }

    @Test
    public void onAppStartShouldStartMusicWhenEnabled() throws Throwable {
        NavigationPaneView view = new NavigationPaneView(new WeakReference<>(activity), R.id.navigationPaneRootView);

        when(preferences.getIsMusicEnabled()).
            thenReturn(true);

        presenter = new NavigationPanePresenter.Builder()
            .view(view)
            .preferences(preferences)
            .music(music)
            .build();

        view.setPresenter(presenter);

        view.onAppActive();
        verify(music).play();
    }

    @Test
    public void onAppStartShouldNotStartMusicWhenDisabled() {
        NavigationPaneView view = new NavigationPaneView(new WeakReference<>(activity), R.id.navigationPaneRootView);

        when(preferences.getIsMusicEnabled()).
            thenReturn(false);

        presenter = new NavigationPanePresenter.Builder()
            .view(view)
            .preferences(preferences)
            .music(music)
            .build();

        view.setPresenter(presenter);

        view.onAppActive();
        verifyZeroInteractions(music);
    }

    @Test
    public void onAppExitShouldStopMusic() throws Throwable {
        NavigationPaneView view = new NavigationPaneView(new WeakReference<>(activity), R.id.navigationPaneRootView);

        presenter = new NavigationPanePresenter.Builder()
            .view(view)
            .music(music)
            .build();

        view.setPresenter(presenter);

        view.onAppInactive();
        verify(music).stop();
    }

    @Test
    public void onAppExitShouldStopSpeaking() throws Throwable {
        NavigationPaneView view = new NavigationPaneView(new WeakReference<>(activity), R.id.navigationPaneRootView);

        presenter = new NavigationPanePresenter.Builder()
            .view(view)
            .speaking(speaking)
            .build();

        view.setPresenter(presenter);

        view.onAppInactive();
        verify(speaking).stop();
    }

    @Test
    public void onClickVersesPaneBtnShouldLoadVersesPane() throws Throwable {
        presenter = new NavigationPanePresenter.Builder()
            .view(mockedView)
            .build();

        executeOnUiThread(() -> presenter.onClickVersesPaneBtn());

        verify(mockedView).loadPane(PaneType.Verses);
    }

    @Test
    public void onClickWallpapersPaneBtnShouldLoadWallpapersPane() throws Throwable {
        presenter = new NavigationPanePresenter.Builder()
            .view(mockedView)
            .build();

        executeOnUiThread(() -> presenter.onClickWallpapersPaneBtn());

        verify(mockedView).loadPane(PaneType.Wallpapers);
    }
}