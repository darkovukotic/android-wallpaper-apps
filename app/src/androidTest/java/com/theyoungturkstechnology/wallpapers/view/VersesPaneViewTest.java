package com.theyoungturkstechnology.wallpapers.view;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.theyoungturkstechnology.wallpapers.R;
import com.theyoungturkstechnology.wallpapers.presenter.VersesPaneContract.Presenter;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.lang.ref.WeakReference;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

/**
 * Created by dvukotic on 05.01.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
@android.support.test.filters.LargeTest
@RunWith(AndroidJUnit4.class)
public class VersesPaneViewTest {
    @Rule public final ExpectedException exception = ExpectedException.none();
    @Rule public MockitoRule mockito = MockitoJUnit.rule();
    @Rule public ActivityTestRule<MainActivity> testActivity = new ActivityTestRule<>(MainActivity.class);

    private MainActivity activity;
    @Mock private Presenter presenter;

    @Before
    public void setUp() throws Exception {
        activity = testActivity.getActivity();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void onViewResumePresenterWillBeCalled() throws Throwable {
        VersesPaneView view = new VersesPaneView(new WeakReference<>(activity), R.id.versesPaneRootView);
        view.setPresenter(presenter);

        testActivity.runOnUiThread(()->
            {
                view.load();
                verify(presenter).viewDidShow();
            }
        );
    }

    @Test
    public void onClickShareVerses() {
        VersesPaneView view = new VersesPaneView(new WeakReference<>(activity), R.id.versesPaneRootView);
        view.setPresenter(presenter);

        activity.findViewById(R.id.shareVersesBtn).callOnClick();
        verify(presenter).onClickShareVersesBtn();
    }

    @Test
    public void onClickReadVerses() {
        VersesPaneView view = new VersesPaneView(new WeakReference<>(activity), R.id.versesPaneRootView);
        view.setPresenter(presenter);

        activity.findViewById(R.id.readVersesBtn).callOnClick();
        verify(presenter).onClickReadVersesBtn();
    }

    @Test
    public void onClickNextVerses() {
        VersesPaneView view = new VersesPaneView(new WeakReference<>(activity), R.id.versesPaneRootView);
        view.setPresenter(presenter);

        activity.findViewById(R.id.nextVersesBtn).callOnClick();
        verify(presenter).onClickNextVersesBtn();
    }
}