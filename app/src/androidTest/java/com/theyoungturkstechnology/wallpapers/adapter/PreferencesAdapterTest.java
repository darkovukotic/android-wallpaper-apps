package com.theyoungturkstechnology.wallpapers.adapter;

import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.theyoungturkstechnology.wallpapers.adapter.PreferencesAdapter;
import com.theyoungturkstechnology.wallpapers.adapter.PreferencesAdapterObject;
import com.theyoungturkstechnology.wallpapers.view.MainActivity;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.lang.ref.WeakReference;

import static org.junit.Assert.*;
import static org.mockito.Mockito.spy;

/**
 * Created by dvukotic on 05.01.2018.
 */

@LargeTest
@RunWith(AndroidJUnit4.class)
@SuppressWarnings("DefaultFileTemplate")
public class PreferencesAdapterTest {
    @Rule public MockitoRule mockito = MockitoJUnit.rule();
    @Rule public ActivityTestRule<MainActivity> testActivity = new ActivityTestRule<>(MainActivity.class);

    private MainActivity activity;
    private PreferencesAdapter preferences = null;

    @Before
    public void setUp() throws Exception {
        activity = testActivity.getActivity();
        preferences = new PreferencesAdapterObject(new WeakReference<>(activity));
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void lastWallpaperIndexWrittenToShouldBeReadFromPreferences() {
        int indices[] = new int[] {1, 2, 5, 101, 999999, 0};

        for (int index: indices) {
            preferences.saveLastSelectedWallpaperIndex(index);
            assertEquals(index, preferences.getLastSelectedWallpaperIndex());
        }
    }

    @Test
    public void defaultWallpaperIndexValueShouldBeZero() {
        int index = preferences.getLastSelectedWallpaperIndex();
        assertEquals(0,index);
    }

    @Test
    public void lastWallpaperIndexWrittenToShouldBeReadFromPreferencesAgainAndAgain() {
        int index = 3;

        preferences.saveLastSelectedWallpaperIndex(index);
        assertEquals(index, preferences.getLastSelectedWallpaperIndex());
        assertEquals(index, preferences.getLastSelectedWallpaperIndex());
        assertEquals(index, preferences.getLastSelectedWallpaperIndex());
        assertEquals(index, preferences.getLastSelectedWallpaperIndex());
    }

    @Test
    public void defaultMusicEnabledValueShouldBeTrue() {
        boolean enabled = preferences.getIsMusicEnabled();
        assertEquals(true, enabled);
    }

    @Test
    public void lastMusicEnabledValueWrittenToShouldBeReadFromPreferences() {
        boolean values[] = new boolean[] { false, true, true, false, true };

        for (Boolean enabled: values) {
            preferences.saveIsMusicEnabled(enabled);
            assertEquals(enabled, preferences.getIsMusicEnabled());
        }
    }

    @Test
    public void lastLanguageCodeWrittenToShouldBeReadFromPreferences() {
        String values[] = new String[] { "en", "tr", "sr", "ar" };

        for (String code: values) {
            preferences.saveCurrentLanguageCode(code);
            assertEquals(code, preferences.getCurrentLanguageCode());
        }
    }
}