package com.theyoungturkstechnology.wallpapers.adapter;

import android.os.SystemClock;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.theyoungturkstechnology.wallpapers.adapter.SpeakingAdapter;
import com.theyoungturkstechnology.wallpapers.provider.Provider;
import com.theyoungturkstechnology.wallpapers.view.MainActivity;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import java.util.Locale;

import static org.junit.Assert.*;

/**
 * Created by dvukotic on 30.01.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
@RunWith(AndroidJUnit4.class)
@LargeTest
public class SpeakingAdapterTest {
    @Rule public final ExpectedException exception = ExpectedException.none();
    @Rule public ActivityTestRule<MainActivity> testActivity = new ActivityTestRule<>(MainActivity.class);

    private SpeakingAdapter speaking;
    private final int WAIT_FOR_START_OF_SPEAKING = 3200;
    private final int WAIT_FOR_END_OF_SPEAKING = 200;

    @Before
    public void setUp() throws Exception {
        Provider provider = Provider.instance();
        speaking = provider.provideSpeakingAdapter();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void usLocaleShouldBeSupported() throws Throwable {
        exception.expect(AssertionError.class);
        speaking.isLocaleSupported(
            Locale.US,
            () -> {
                // if we are here it's supported
                throw new AssertionError();
            },
            () -> {
                // if we are here it's not supported
                assertTrue(false);
            }
        );
    }

    @Test
    public void azLocaleShouldNotBeSupported() throws Throwable {
        exception.expect(AssertionError.class);
        speaking.isLocaleSupported(
            Locale.forLanguageTag("az"),
            () -> {
                // if we are here it's supported
                assertTrue(false);
            },
            () -> {
                // if we are here it's not supported
                throw new AssertionError();
            }
        );
    }

    @Test
    public void usLocaleShouldBeSupportedInStart() throws Throwable {
        speaking.start(
            "a",
            Locale.US,
            () -> {
                // if we are here we have an error (language not supported etc)
                assertTrue(false);
            }
        );
    }

    @Test
    public void azLocaleShouldNotBeSupportedInStart() throws Throwable {
        exception.expect(AssertionError.class);
        speaking.start(
            "a",
            Locale.forLanguageTag("az"),
            () -> {
                // if we are here we have an error (language not supported etc)
                throw new AssertionError();
            }
        );
    }

    @Test
    public void shouldSpeakAndIsSpeakingShouldWork() throws Throwable {
        speaking.stop();
        SystemClock.sleep(WAIT_FOR_END_OF_SPEAKING);
        assertFalse(speaking.isSpeaking());

        speaking.start(
            "how do you do, how do you do, how do you do",
            Locale.US,
            () -> {
                // if we are here we have an error (language not supported etc)
                assertTrue(false);
            }
        );

        SystemClock.sleep(WAIT_FOR_START_OF_SPEAKING);
        assertTrue(speaking.isSpeaking());
    }

    @Test
    public void shouldStopSpeakingOnStop() throws Throwable {
        speaking.stop();
        SystemClock.sleep(WAIT_FOR_END_OF_SPEAKING);
        assertFalse(speaking.isSpeaking());

        speaking.start(
            "how do you do, how do you do, how do you do, how do you do, how do you do, how do you do, how do you do," +
                "how do you do, how do you do, how do you do, how do you do, how do you do, how do you do, how do you do" +
                "how do you do, how do you do, how do you do, how do you do, how do you do, how do you do, how do you do",
            Locale.US,
            () -> {
                // if we are here we have an error (language not supported etc)
                assertTrue(false);
            }
        );

        SystemClock.sleep(WAIT_FOR_START_OF_SPEAKING);
        assertTrue(speaking.isSpeaking());

        SystemClock.sleep(2000);

        speaking.stop();
        SystemClock.sleep(WAIT_FOR_END_OF_SPEAKING);
        assertFalse(speaking.isSpeaking());
    }

    @Test
    public void severalStopsOneAfterAnotherShouldntCrashIt() throws Throwable {
        speaking.stop();
        SystemClock.sleep(WAIT_FOR_END_OF_SPEAKING);
        assertFalse(speaking.isSpeaking());

        speaking.start(
            "how do you do, how do you do, how do you do, how do you do, how do you do, how do you do, how do you do," +
                "how do you do, how do you do, how do you do, how do you do, how do you do, how do you do, how do you do",
            Locale.US,
            () -> {
                // if we are here we have an error (language not supported etc)
                assertTrue(false);
            }
        );

        SystemClock.sleep(WAIT_FOR_START_OF_SPEAKING);
        assertTrue(speaking.isSpeaking());

        SystemClock.sleep(2000);
        assertTrue(speaking.isSpeaking());

        speaking.stop();
        SystemClock.sleep(WAIT_FOR_END_OF_SPEAKING);
        assertFalse(speaking.isSpeaking());

        speaking.stop();
        speaking.stop();
        speaking.stop();
        assertFalse(speaking.isSpeaking());
    }

    @Test
    public void severalStartsOneAfterAnotherShouldStopPreviousAndStartNewOne() throws Throwable {
        speaking.stop();
        SystemClock.sleep(WAIT_FOR_END_OF_SPEAKING);
        assertFalse(speaking.isSpeaking());

        speaking.start(
            "how do you do, how do you do, how do you do, how do you do, how do you do, how do you do, how do you do," +
                "how do you do, how do you do, how do you do, how do you do, how do you do, how do you do, how do you do",
            Locale.US,
            () -> {
                // if we are here we have an error (language not supported etc)
                assertTrue(false);
            }
        );

        SystemClock.sleep(2000);
        assertTrue(speaking.isSpeaking());

        speaking.start(
            "la ba da, la ba da, la ba da, la ba da, la ba da, la ba da, la ba da, la ba da, la ba da, la ba da, " +
                "la ba da, la ba da, la ba da, la ba da, la ba da, la ba da, la ba da, la ba da, la ba da, la ba da, la ba da, la ba da",
            Locale.US,
            () -> {
                // if we are here we have an error (language not supported etc)
                assertTrue(false);
            }
        );

        SystemClock.sleep(2000);
        assertTrue(speaking.isSpeaking());

        speaking.start(
            "partizan rules, partizan rules, partizan rules, partizan rules, partizan rules, partizan rules, partizan rules, " +
                "partizan rules, partizan rules, partizan rules, partizan rules, partizan rules, partizan rules, partizan rules",
            Locale.US,
            () -> {
                // if we are here we have an error (language not supported etc)
                assertTrue(false);
            }
        );

        SystemClock.sleep(2000);
        assertTrue(speaking.isSpeaking());

        speaking.stop();
        SystemClock.sleep(WAIT_FOR_END_OF_SPEAKING);
        assertFalse(speaking.isSpeaking());
    }

    @Test
    public void shouldStopOnLeavingActivity() throws Exception {
        speaking.stop();
        SystemClock.sleep(WAIT_FOR_END_OF_SPEAKING);
        assertFalse(speaking.isSpeaking());

        speaking.start(
            "how do you do, how do you do, how do you do, how do you do, how do you do, how do you do, how do you do," +
                "how do you do, how do you do, how do you do, how do you do, how do you do, how do you do, how do you do",
            Locale.US,
            () -> {
                // if we are here we have an error (language not supported etc)
                assertTrue(false);
            }
        );

        SystemClock.sleep(2000);
        assertTrue(speaking.isSpeaking());

        testActivity.finishActivity();
        SystemClock.sleep(200);
        assertFalse(speaking.isSpeaking());
    }

}