package com.theyoungturkstechnology.wallpapers.adapter;

import android.content.Intent;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.theyoungturkstechnology.wallpapers.adapter.MusicAdapter;
import com.theyoungturkstechnology.wallpapers.provider.Provider;
import com.theyoungturkstechnology.wallpapers.view.MainActivity;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

/**
 * Created by dvukotic on 25.01.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
@RunWith(AndroidJUnit4.class)
@LargeTest
public class MusicAdapterTest {
    @Rule public final ExpectedException exception = ExpectedException.none();
    @Rule public ActivityTestRule<MainActivity> testActivity = new ActivityTestRule<>(MainActivity.class);
    private MusicAdapter music;

    @Before
    public void setUp() throws Exception {
        Provider provider = Provider.instance();
        music = provider.provideMusicAdapter();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void startStopTheMusic() throws Exception {
        // we have it working because we started activity
        music.stop();
        assertEquals(false, music.isPlaying());

        music.play();
        assertEquals(true, music.isPlaying());
        Thread.sleep(5*1000);
        music.stop();
        assertEquals(false, music.isPlaying());

        Thread.sleep(3*1000);

        music.play();
        assertEquals(true, music.isPlaying());
        Thread.sleep(5*1000);
        music.stop();
        assertEquals(false, music.isPlaying());
    }

    @Test
    public void shouldNotCrashOnMultipleStop() throws Exception {
        // we have it working because we started activity
        music.stop();
        assertEquals(false, music.isPlaying());

        music.play();
        assertEquals(true, music.isPlaying());
        Thread.sleep(5*1000);
        music.stop();
        music.stop();
        music.stop();
        assertEquals(false, music.isPlaying());
    }

    @Test
    public void shouldNotCrashOnMultiplePlay() throws Exception {
        // we have it working because we started activity
        music.stop();
        assertEquals(false, music.isPlaying());

        music.play();
        music.play();
        Thread.sleep(1000);
        music.play();
        assertEquals(true, music.isPlaying());
        Thread.sleep(5*1000);
        music.stop();
        assertEquals(false, music.isPlaying());
    }

    @Test
    public void shouldNotInterruptMusicOnMultiplePlay() throws Exception {
        // we have it working because we started activity
        music.stop();
        assertEquals(false, music.isPlaying());

        music.play();
        assertEquals(true, music.isPlaying());
        Thread.sleep(2*1000);
        music.play();
        assertEquals(true, music.isPlaying());
        Thread.sleep(2*1000);
        music.play();
        assertEquals(true, music.isPlaying());
        Thread.sleep(2*1000);
        music.stop();
        assertEquals(false, music.isPlaying());
    }

    @Test
    public void shouldNotStutterOnMultipleStartStop() throws Exception {
        // we have it working because we started activity
        music.stop();
        assertEquals(false, music.isPlaying());

        music.play();
        assertEquals(true, music.isPlaying());
        Thread.sleep(2*1000);
        music.stop();
        assertEquals(false, music.isPlaying());
        music.play();
        assertEquals(true, music.isPlaying());
        Thread.sleep(2*1000);
        music.stop();
        assertEquals(false, music.isPlaying());
        music.play();
        assertEquals(true, music.isPlaying());
        Thread.sleep(2*1000);
        music.stop();
        assertEquals(false, music.isPlaying());
        music.play();
        assertEquals(true, music.isPlaying());
        Thread.sleep(2*1000);
        music.stop();
        assertEquals(false, music.isPlaying());
    }

    @Test
    public void shouldStopOnLeavingActivity() throws Exception {
        // we have it working because we started activity
        assertEquals(true, music.isPlaying());
        Thread.sleep(5*1000);

        testActivity.finishActivity();
        Thread.sleep(500);
        assertEquals(false, music.isPlaying());
    }

    @Test
    public void shouldStopOnLeavingActivityAndContinueOnReturningBack() throws Exception {
        // we have it working because we started activity
        assertEquals(true, music.isPlaying());
        Thread.sleep(5*1000);

        Intent intent = testActivity.getActivity().getIntent();

        testActivity.finishActivity();
        Thread.sleep(200);
        assertEquals(false, music.isPlaying());
        Thread.sleep(3*1000);

        testActivity.launchActivity(intent);
        assertEquals(true, music.isPlaying());
        Thread.sleep(5*1000);
    }

}
