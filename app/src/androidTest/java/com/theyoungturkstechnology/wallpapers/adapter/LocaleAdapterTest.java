package com.theyoungturkstechnology.wallpapers.adapter;

import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.theyoungturkstechnology.wallpapers.model.FontCode;
import com.theyoungturkstechnology.wallpapers.model.LanguageModel;
import com.theyoungturkstechnology.wallpapers.provider.Provider;
import com.theyoungturkstechnology.wallpapers.adapter.LocaleAdapter;
import com.theyoungturkstechnology.wallpapers.view.MainActivity;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import java.util.Locale;

import static org.junit.Assert.*;

/**
 * Created by dvukotic on 29.01.2018.
 */

@SuppressWarnings("DefaultFileTemplate")
@LargeTest
@RunWith(AndroidJUnit4.class)
public class LocaleAdapterTest {
    @Rule public final ExpectedException exception = ExpectedException.none();
    @Rule public ActivityTestRule<MainActivity> testActivity = new ActivityTestRule<>(MainActivity.class);
    private LocaleAdapter tools;

    @Before
    public void setUp() throws Exception {
        Provider provider = Provider.instance();
        tools = provider.provideLocaleAdapter();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void shouldReturnLocaleForLanguage() throws Exception {
        LanguageModel language = new LanguageModel("sr", "....", FontCode.CyrillicFont);
        Locale locale = tools.getLocaleForLanguageCode(language.getCode());
        assertNotNull(locale);
        assertEquals("Serbian", locale.getDisplayName());
    }

    @Test
    public void shouldReturnPhoneLanguageCode() throws Exception {
        String code = tools.getPhoneLanguageCode();
        assertEquals("en", code);
    }

}